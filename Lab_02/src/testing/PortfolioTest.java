package testing;

import tracking.*;
import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;

class PortfolioTest {
	
	@Test // This test is for testing the construction of the Portfolio class.
	void testConstruction() {
		Portfolio p1 = new Portfolio(new Owner("", ""));
		Portfolio p2 = new Portfolio( new Owner[] {
				new Owner("", ""),
				new Owner("", ""),
				new Owner("", "")
			}	
		);
	}
	
	@Test // This test is for testing the getCdByName method of the Portfolio class.
	void testGetCdByName() {
		Date d = new Date(1336194000000l);

		Portfolio p1 = new Portfolio(new Owner("Carl", "Johnson"));
		Portfolio p2 = new Portfolio( new Owner[] {
				new Owner("Tim", "Thompson"),
				new Owner("Man", "Person"),
				new Owner("Dude", "Bobson")
			}	
		);
		CD cd1 = new CD( p1, "TEST1", 2500000, d, 5, 25, CD.CD_COMPOUND_DAILY);
		CD cd2 = new CD( p1, "TEST2", 2500000, d, 5, 25, CD.CD_COMPOUND_DAILY);
		
		p1.addCdToPortfolio(cd1);
		assertEquals(cd1, p1.getCdByName("TEST1"));
		assertEquals(null, p1.getCdByName("TEST9000"));
		
		p2.addCdToPortfolio(cd1);
		p2.addCdToPortfolio(cd2);
		assertEquals(cd1, p2.getCdByName("TEST1"));
		assertEquals(cd2, p2.getCdByName("TEST2"));
		assertEquals(null, p2.getCdByName("TEST9000"));
	}
	
	@Test // This test is for testing the findCDWithMaxMaturityValue method of the Portfolio class.
	void testFindCDWithMaxMaturityValue() {
		Portfolio p1 = new Portfolio(new Owner("Carl", "Johnson"));
		CD cd1 = new CD(p1, "TEST", 100000l, new Date(1420092000000l), 12, 0.015f, CD.CD_COMPOUND_ANNUALLY);
		CD cd2 = new CD(p1, "TEST", 100000l, new Date(1426395600000l), 60, 0.0235f, CD.CD_COMPOUND_ANNUALLY);
		CD cd3 = new CD(p1, "TEST", 100000l, new Date(1433134800000l), 60, 0.0235f, CD.CD_COMPOUND_MONTHLY);
		CD cd4 = new CD(p1, "TEST", 100000l, new Date(1451628000000l), 60, 0.0235f, CD.CD_COMPOUND_DAILY);
		p1.addCdToPortfolio(cd1);
		p1.addCdToPortfolio(cd2);
		p1.addCdToPortfolio(cd3);
		p1.addCdToPortfolio(cd4);
		assertEquals(cd4, p1.findCDWithMaxMaturityValue());
	}
	
	@Test // This test is for testing the findCDMaturingSoonest method of the Portfolio class.
	void testFindCDMaturingSoonest() {
		Portfolio p1 = new Portfolio(new Owner("Carl", "Johnson"));
		CD cd1 = new CD(p1, "TEST", 100000l, new Date(1420092000000l), 12, 0.015f, CD.CD_COMPOUND_ANNUALLY);
		CD cd2 = new CD(p1, "TEST", 100000l, new Date(1426395600000l), 60, 0.0235f, CD.CD_COMPOUND_ANNUALLY);
		CD cd3 = new CD(p1, "TEST", 100000l, new Date(1433134800000l), 60, 0.0235f, CD.CD_COMPOUND_MONTHLY);
		CD cd4 = new CD(p1, "TEST", 100000l, new Date(1451628000000l), 60, 0.0235f, CD.CD_COMPOUND_DAILY);
		p1.addCdToPortfolio(cd1);
		p1.addCdToPortfolio(cd2);
		p1.addCdToPortfolio(cd3);
		p1.addCdToPortfolio(cd4);
		assertEquals(cd1, p1.findCDMaturingSoonest());
	}
	
	@Test // This test is for testing the generatePortfolioSummaryReport method of the Portfolio class.
	void testGeneratePortfolioSummaryReport() {

		Portfolio p1 = new Portfolio(new Owner("Bob", "Smith"));
		CD cd1 = new CD(p1, "CD_01", 100000l, new Date(1420092000000l), 12, 0.015f, CD.CD_COMPOUND_ANNUALLY);
		CD cd2 = new CD(p1, "CD_02", 100000l, new Date(1426395600000l), 60, 0.0235f, CD.CD_COMPOUND_ANNUALLY);
		CD cd3 = new CD(p1, "CD_03", 100000l, new Date(1433134800000l), 60, 0.0235f, CD.CD_COMPOUND_MONTHLY);
		CD cd4 = new CD(p1, "CD_04", 100000l, new Date(1451628000000l), 60, 0.0235f, CD.CD_COMPOUND_DAILY);
		p1.addCdToPortfolio(cd1);
		p1.addCdToPortfolio(cd2);
		p1.addCdToPortfolio(cd3);
		p1.addCdToPortfolio(cd4);
		
		p1.generatePortfolioSummaryReport();
	}
}
