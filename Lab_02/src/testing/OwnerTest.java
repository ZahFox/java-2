package testing;

import tracking.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class OwnerTest {

	@Test // This test is for testing the setters, getters, and toString method for the Owner class.
	void testOwner() {
		Owner o = new Owner("/", " ");
		assertEquals(" , /", o.toString());
		assertEquals("/", o.getFirstName());
		assertEquals(" ", o.getLastName());
		o.setFirstName("Bob");
		o.setLastName("Smith");
		assertEquals("Bob", o.getFirstName());
		assertEquals("Smith", o.getLastName());
		assertEquals("Smith, Bob", o.toString());
	}
}
