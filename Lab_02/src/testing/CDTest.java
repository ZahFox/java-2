/* QUESTIONS
 * 1. Do we still need the monthsPenalty property in the CD class?
 * 2. How should we implement the toString method for the Portfolio class?
 */

package testing;

import tracking.*;
import static org.junit.jupiter.api.Assertions.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.Test;

class CDTest {
	
	private static final DateFormat df = new SimpleDateFormat("M/d/yyyy");
	private static final Portfolio p = new Portfolio(new Owner("Carl", "Johnson"));
	
	@Test // This test is for testing the setters and getters for the CD class.
	void testAccessors() {
		Date d = new Date(1336194000000l);
		CD cd = new CD(p, "TEST", 2500000, d, 5, 25, CD.CD_COMPOUND_DAILY);
		
		// Testing Getters after Construction
		assertEquals("TEST", cd.getCdName());
		assertEquals(2500000, cd.getAmountInCents());
		assertEquals(new Date(1336194000000l), cd.getPurchaseDate());
		assertEquals(5, cd.getMonths());
		assertEquals(25, cd.getAnnualRate());
		assertEquals(365, cd.getCompoundingType());
		    // assertEquals(5, cd.getMonthsPenalty());
		
		// Testing Setters
		cd.setCdName("TEST!");
		cd.setAmountInCents(9000);
		cd.setPurchaseDate(new Date(1436194000000l));
		cd.setMonths(42);
		cd.setAnnualRate(42);
		cd.setCompoundingType(CD.CD_COMPOUND_MONTHLY);
		    // cd.setMonthsPenalty(42);
		
		// Testing Getters after using Setters
		assertEquals("TEST!", cd.getCdName());
		assertEquals(9000, cd.getAmountInCents());
		assertEquals(new Date(1436194000000l), cd.getPurchaseDate());
		assertEquals(42, cd.getMonths());
		assertEquals(42, cd.getAnnualRate());
		assertEquals(12, cd.getCompoundingType());
		    // assertEquals(42, cd.getMonthsPenalty());
	}
	
	@Test // This test is for Calculating the Value at Maturity
	void testCalcValueAtMaturity() {
		Date d = new Date(1420092000000l); // "01/01/2015"
		CD cd = new CD(p, "TEST", 100000l, d, 12, 0.015f, CD.CD_COMPOUND_ANNUALLY);
		assertEquals(1015, cd.calcValueAtMaturity()/100);
	}
	
	@Test // Test Cases_v2.xlsx Test Case 1
	void testCase1() {
		Date d = new Date(1420092000000l); // "01/01/2015"
		CD cd = new CD(p, "TEST", 100000l, d, 12, 0.015f, CD.CD_COMPOUND_ANNUALLY);
		assertEquals("1/1/2015", df.format(cd.getPurchaseDate()));
		assertEquals("1/1/2016", df.format(cd.calcMaturityDate()));
		assertEquals(1015.00f, ( ((float)cd.calcValueAtMaturity()) / 100 ));
		assertEquals(15.00f, ( ((float)(cd.calcValueAtMaturity() - cd.getAmountInCents())) / 100 ));
	}
	
	@Test // Test Cases_v2.xlsx Test Case 2
	void testCase2() {
		Date d = new Date(1426395600000l); // "03/15/2015"
		CD cd = new CD(p, "TEST", 100000l, d, 60, 0.0235f, CD.CD_COMPOUND_ANNUALLY);
		assertEquals("3/15/2015", df.format(cd.getPurchaseDate()));
		assertEquals("3/15/2020", df.format(cd.calcMaturityDate()));
		assertEquals(1123.30f, ( ((float)cd.calcValueAtMaturity()) / 100 ));
		assertEquals(123.30f, ( ((float)(cd.calcValueAtMaturity() - cd.getAmountInCents())) / 100 ));
	}
	
	@Test // Test Cases_v2.xlsx Test Case 3
	void testCase3() {
		Date d = new Date(1433134800000l); // "06/01/2015"
		CD cd = new CD(p, "TEST", 100000l, d, 60, 0.0235f, CD.CD_COMPOUND_MONTHLY);
		assertEquals("6/1/2015", df.format(cd.getPurchaseDate()));
		assertEquals("6/1/2020", df.format(cd.calcMaturityDate()));
		assertEquals(1124.70f, ( ((float)cd.calcValueAtMaturity()) / 100 ));
		assertEquals(124.70f, ( ((float)(cd.calcValueAtMaturity() - cd.getAmountInCents())) / 100 ));
	}
	
	@Test // Test Cases_v2.xlsx Test Case 4
	void testCase4() {
		Date d = new Date(1451628000000l); // "01/01/2016"
		CD cd = new CD(p, "TEST", 100000l, d, 60, 0.0235f, CD.CD_COMPOUND_DAILY);
		assertEquals("1/1/2016", df.format(cd.getPurchaseDate()));
		assertEquals("1/1/2021", df.format(cd.calcMaturityDate()));
		assertEquals(1124.82f, ( ((float)cd.calcValueAtMaturity()) / 100 ));
		assertEquals(124.82f, ( ((float)(cd.calcValueAtMaturity() - cd.getAmountInCents())) / 100 ));
	}
	
	@Test // This test is for testing the toString method of the CD class.
	void testToString() {
		Date d = new Date(1451628000000l); // "01/01/2016"
		CD cd = new CD(p, "CD_04", 100000l, d, 60, 0.0235f, CD.CD_COMPOUND_DAILY);
		assertEquals("CD Name: CD_04\nAmount Invested: $1,000.00\nPurchase Date: 1/1/2016\nTerm(Months): 60\nAnnual Rate: 2.35%\nCompounding Type: Daily\nMaturity Date: 1/1/2021\nValue at Maturity: $1,124.82\nInterest Earned at Maturity: $124.82\n",
				cd.toString()
		);
	}
	
}
