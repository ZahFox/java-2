package tracking;

public class Owner {

	// Instance Member Variables
	private String firstName;
	private String lastName;
	private Portfolio portfolio;
	
	
	// Constructor
	public Owner(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	
	// Other Methods
	public void assignPortfolioToOwner(Portfolio p) {
		if (p != null) this.portfolio = p;
	}
	
	@Override
	public String toString() {
		return String.format("%s, %s", this.lastName, this.firstName);
	}
	
	
	// Accessor Methods
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}
