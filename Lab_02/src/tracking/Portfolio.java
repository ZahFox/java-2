package tracking;

import java.util.ArrayList;
import java.util.Arrays;

public class Portfolio {

	// Instance Member Variables
	private ArrayList<Owner> ownerList;
	private ArrayList<CD> cdList;
	
	
	// Constructors
	public Portfolio(Owner o) {
		this.ownerList = new ArrayList<Owner>(Arrays.asList(o));
		this.cdList = new ArrayList<CD>();
	}
	
	public Portfolio(Owner[] owners) {
		this.ownerList = new ArrayList<Owner>(Arrays.asList(owners));
		this.cdList = new ArrayList<CD>();
	}
	
	
	// Other Methods
	public void addOwnerToPortfolio(Owner o) {
		if (o != null) this.ownerList.add(o);
	}
	
	public void addCdToPortfolio(CD cd) {
		if (cd != null) {
			cd.setPortfolio(this);
			this.cdList.add(cd);
		}
	}
	
	public CD getCdByName(String cdName) {
		for (int i = 0; i < this.cdList.size(); i++) {
			if (this.cdList.get(i).getCdName() == cdName)
				return this.cdList.get(i);
		}
		return null;
	}
	
	public CD findCDWithMaxMaturityValue() {
		long max = -1;
		CD maxCD = null;
		for (int i = 0; i < this.cdList.size(); i++) {
			long value = this.cdList.get(i).calcValueAtMaturity();
			if (value > max) {
				max = value;
				maxCD = this.cdList.get(i);
			}
		}
		return maxCD; 
	}
	
	public CD findCDMaturingSoonest() {
		long min = -1;
		CD minTimeCD = null;
		for (int i = 0; i < this.cdList.size(); i++) {
			long time = this.cdList.get(i).calcMaturityDate().getTime();
			if (min == -1 || time < min) {
				min = time;
				minTimeCD = this.cdList.get(i);
			}
		}
		return minTimeCD;
	}
	
	public void generatePortfolioSummaryReport() {
		String report = "***Portfolio Summary Report***\n\n***Owners***\n";
		// Concatenate Owners
		for (int i = 0; i < this.ownerList.size(); i++) {
			report += "Owner " + (i+1) + ": " + this.ownerList.get(i).toString() + '\n';
		}
		
		// Concatenate CDs
		report += "\n***CDs***\n";
		for (int i = 0; i < this.cdList.size(); i++) {
			report += this.cdList.get(i).toString() + '\n';
		}
		
		// Concatenate Max Value and Soonest Maturity CDs
		report += "\n***CD with Max Value at Maturity***\n" +
		this.findCDWithMaxMaturityValue().toString() +
		"\n***CD that is Maturing the Soonest***\n" +
		this.findCDMaturingSoonest().toString();
		
		// Print Report to Console
		System.out.print(report);
	}
	
	
	// Accessor Methods
	public ArrayList<Owner> getOwnerList() {
		return ownerList;
	}

	public void setOwnerList(ArrayList<Owner> ownerList) {
		this.ownerList = ownerList;
	}

	public ArrayList<CD> getCdList() {
		return cdList;
	}

	public void setCdList(ArrayList<CD> cdList) {
		this.cdList = cdList;
	}
}
