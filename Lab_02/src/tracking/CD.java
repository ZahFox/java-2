package tracking;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.lang.Math;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CD {
	
	// Static Member Variables
	public static final int CD_COMPOUND_ANNUALLY = 1;
	public static final int CD_COMPOUND_MONTHLY = 12;
	public static final int CD_COMPOUND_DAILY = 365;
	
	
	// Instance Member Variables
	private String cdName;
	private long amountInCents;
	private Date purchaseDate;
	private int months;
	private double annualRate;
	private int compoundingType;
	private Portfolio portfolio;
	
	
	// Constructor
	public CD(Portfolio p, String cdName, long amountInCents, Date purchaseDate, int months,
			  double annualRate, int compoundingType) {
		this.cdName = cdName;
		this.amountInCents = amountInCents;
		this.purchaseDate = purchaseDate;
		this.months = months;
		this.annualRate = annualRate;
		this.compoundingType = compoundingType;
	}
	
	
	
	// Other Methods
	public Date calcMaturityDate() {
		Calendar c = Calendar.getInstance();
		c.setTime(this.purchaseDate);
		c.add(Calendar.MONTH, this.months);
		return new Date(c.getTimeInMillis());
	}
	
	public long calcValueAtMaturity() {
		// FV = PV * (1 + r/n)^n*t
		float years = (this.calcMaturityDate().getTime() - this.purchaseDate.getTime()) / 31536000000f;

		return Math.round(
		    this.amountInCents *
		    Math.pow( (1+(this.annualRate / this.compoundingType)) , (this.compoundingType*years) )
		);
	}
	
	@Override
	public String toString() {
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		DateFormat df = new SimpleDateFormat("M/d/yyyy");
		String compType = "";
		long maturityVal = this.calcValueAtMaturity();
		
		switch(this.compoundingType) {
			case CD_COMPOUND_ANNUALLY:
				compType = "Annually";
				break;
			case CD_COMPOUND_MONTHLY:
				compType = "Monthly";
				break;
			case CD_COMPOUND_DAILY:
				compType = "Daily";
				break;
		}
		
		return String.format(
				"CD Name: %s\nAmount Invested: %s\nPurchase Date: %s\n"
				+ "Term(Months): %d\nAnnual Rate: %.2f%%\nCompounding Type: %s\n"
				+ "Maturity Date: %s\nValue at Maturity: %s\n"
				+ "Interest Earned at Maturity: %s\n",
			this.cdName, nf.format(((float)this.amountInCents) / 100),
			df.format(this.purchaseDate), this.months, this.annualRate * 100, compType,
			df.format(this.calcMaturityDate()), nf.format( (float)maturityVal / 100 ),
			nf.format( (float)(maturityVal - this.getAmountInCents()) / 100 )		
		);
	}
	
	
	// Accessor Methods
	public String getCdName() {
		return cdName;
	}
	
	public void setCdName(String cdName) {
		this.cdName = cdName;
	}

	public long getAmountInCents() {
		return amountInCents;
	}

	public void setAmountInCents(long amountInCents) {
		this.amountInCents = amountInCents;
	}

	public Date getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public int getMonths() {
		return months;
	}

	public void setMonths(int months) {
		this.months = months;
	}

	public double getAnnualRate() {
		return annualRate;
	}

	public void setAnnualRate(double annualRate) {
		this.annualRate = annualRate;
	}

	public int getCompoundingType() {
		return compoundingType;
	}

	public void setCompoundingType(int compoundingType) {
		this.compoundingType = compoundingType;
	}

	public Portfolio getPortfolio() {
		return portfolio;
	}

	public void setPortfolio(Portfolio portfolio) {
		this.portfolio = portfolio;
	}
}
