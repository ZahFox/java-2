package tracking;

import java.util.Date;

public class Broker {
	public static void main(String[] args){
		Portfolio p1 = new Portfolio(new Owner("Bob", "Smith"));
		CD cd1 = new CD(p1, "CD_01", 100000l, new Date(1420092000000l), 12, 0.015f, CD.CD_COMPOUND_ANNUALLY);
		CD cd2 = new CD(p1, "CD_02", 100000l, new Date(1426395600000l), 60, 0.0235f, CD.CD_COMPOUND_ANNUALLY);
		CD cd3 = new CD(p1, "CD_03", 100000l, new Date(1433134800000l), 60, 0.0235f, CD.CD_COMPOUND_MONTHLY);
		CD cd4 = new CD(p1, "CD_04", 100000l, new Date(1451628000000l), 60, 0.0235f, CD.CD_COMPOUND_DAILY);
		p1.addCdToPortfolio(cd1);
		p1.addCdToPortfolio(cd2);
		p1.addCdToPortfolio(cd3);
		p1.addCdToPortfolio(cd4);
		
		p1.generatePortfolioSummaryReport();
	}
}
