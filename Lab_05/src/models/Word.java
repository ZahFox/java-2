package models;

public class Word implements Comparable<Word> {

	private String word;
	private int usageCnt;
	
	public Word(String word) {
		this.word = word;
		this.usageCnt = 1;
	}
	
	public Word incrementUsage() {
		this.usageCnt++;
		return this;
	}
	
	public int getCount() {
		return usageCnt;
	}
	
	public String getWord() {
		return word;
	}

	@Override
	public int compareTo(Word other) {
		return (other != null) ? Integer.compare(this.getCount(), other.getCount()) : 1;
	}
	
}
