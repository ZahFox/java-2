package models;

import java.util.Comparator;

public class WordComparator implements Comparator<Word> {

	@Override
	public int compare(Word left, Word right) {
		return left.getWord().compareTo(right.getWord());
	}
	
}
