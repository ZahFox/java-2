package models;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import application.ExecutionPool;
import interfaces.FileConsumer;
import interfaces.FileProducer;

public abstract class AsyncFileParser extends FileParser {
	
	private static final int MULTIPLIER = 2;
	protected static final int MAX_THREADS = Runtime.getRuntime().availableProcessors() * MULTIPLIER;
	private final ExecutionPool pool;
	
	public AsyncFileParser() {
		this.pool = new ExecutionPool(MAX_THREADS);
	}
	
	@Override
	public void Init(FileProducer producer, FileConsumer<?> consumer) {
		super.Init(producer, consumer);
	}
	
	@Override
	public void parse(File file, Optional<Runnable> next) throws IOException, InterruptedException {
		this.producer.produceFile(file, this.consumer);
		
		pool.terminate();
		
		if (next.isPresent()) {
			next.get().run();
		}
	}

	public abstract <T> T[] getData();
	
	protected final void Execute(Runnable task) {
		this.pool.execute(task);
	}
}
