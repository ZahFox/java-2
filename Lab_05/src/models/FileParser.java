package models;

import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.regex.Pattern;

import interfaces.FileConsumer;
import interfaces.FileProducer;

/*
 * This class essentially provides a structure for dynamically creating
 * a file parser with a modular input and output pipeline
 */
public abstract class FileParser {
	
	public enum Parser { ASYNCHASHMAP, SYNCHASHMAP, ASYNCSKIPLIST, SYNCSKIPLIST }
	public enum Sorter { MERGESORT, ARRAYSORT, ARRAYPARALLELSORT }
	
	public static final Pattern WORD_REGEX = Pattern.compile("[^\\w|\\-|']+");
	
	protected FileProducer producer;
	
	// The consumer is generic so that the producer can dictate what data structure
	// the consumer will use to consume the file.
	protected FileConsumer<?> consumer;
	
	public FileParser() { }
	
	public void Init(FileProducer producer, FileConsumer<?> consumer) {
		this.producer = producer;
		this.consumer = consumer;
	}
	
	public void parse(File file, Optional<Runnable> next) throws IOException, InterruptedException {
		this.producer.produceFile(file, this.consumer);
		if (next.isPresent()) {
			next.get().run();
		}
	}
	
	public abstract <T> T[] getData();
}
