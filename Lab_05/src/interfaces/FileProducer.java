package interfaces;

import java.io.File;
import java.io.IOException;

@FunctionalInterface
public interface FileProducer {

	public abstract void produceFile(File path, FileConsumer consumer) throws IOException;
	
}
