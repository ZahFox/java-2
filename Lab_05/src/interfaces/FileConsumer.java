package interfaces;

import java.util.function.Function;

@FunctionalInterface
public interface FileConsumer<T> {
	
	public abstract void consumeFile(T data);
	
}
