package testing;

@FunctionalInterface
public interface ResultHandler<T> {

	public abstract void HandleResult(T result);
	
}
