package testing;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.Map.Entry;

import application.AppDriver;
import application.AsyncHashMapWordParser;
import application.AsyncSkipListMapWordParser;
import application.SyncHashMapWordParser;
import models.FileParser;

public class PerformanceTests {

	private static final int    TOTAL_TESTS = 1000;
	private static final String OUTPUT_FILE = "word_analyzer_performance_results.txt";
	
	public static void main(String[] args) {
		HashMap<String, Long> results = new HashMap<String, Long>();
		
		results.put("ccHashMapAPSTime", ExecuteCycle("ccHashMapAPSTime", new Thread( () ->  {
			AppDriver.ParseFile(new AsyncHashMapWordParser(), FileParser.Sorter.MERGESORT);
		})));
		results.put("ccHashMapGMSTime", ExecuteCycle("ccHashMapGMSTime", new Thread( () ->  {
			AppDriver.ParseFile(new AsyncHashMapWordParser(), FileParser.Sorter.MERGESORT);
		})));
		results.put("ccSkipListAPSTime", ExecuteCycle("ccSkipListAPSTime", new Thread( () ->  {
			AppDriver.ParseFile(new AsyncSkipListMapWordParser(), FileParser.Sorter.ARRAYPARALLELSORT);
		})));
		results.put("ccSkipListGMSTime", ExecuteCycle("ccSkipListGMSTime", new Thread( () ->  {
			AppDriver.ParseFile(new AsyncSkipListMapWordParser(), FileParser.Sorter.MERGESORT);
		})));
		results.put("lHashMapAPSTime", ExecuteCycle("lHashMapAPSTime", new Thread( () ->  {
			AppDriver.ParseFile(new SyncHashMapWordParser(), FileParser.Sorter.ARRAYPARALLELSORT);
		})));
		results.put("lHashMapGMSTime", ExecuteCycle("lHashMapGMSTime", new Thread( () ->  {
			AppDriver.ParseFile(new SyncHashMapWordParser(), FileParser.Sorter.MERGESORT);
		})));	
		
		
		System.out.printf("OPERATING SYSTEM: %s %s %s%n", System.getProperty("os.name"),
				System.getProperty("os.version"), System.getProperty("os.arch"));
		
		PrintResults(results, (result) -> {
			System.out.println(result);
		});
	}


	private static final long GetElapsedTime(String name, Thread task) {
		long beginTime = System.nanoTime();
		
		Execute(name, task);
		
		long endTime = System.nanoTime();
		return endTime - beginTime;
	}
	
	private static final void Execute(String name, Thread task) {
		try {
			task.run();
			task.join();
		} catch (InterruptedException e) {
			System.out.printf(
				"Error: The task: %s, failed to execute properly..\n%s\n",
				name, e.getMessage()
			);
		}
	}
	
	private static final long ExecuteCycle(String name, Thread task) {
		long totalTime = 0;
		
		for (int i = 0; i < TOTAL_TESTS; i++) {
			totalTime += GetElapsedTime(name, task);
		}
		
		return totalTime / TOTAL_TESTS;
	}
	
	private static void PrintResults(HashMap<String, Long> results, ResultHandler<String> handler) {
		for (Entry<String, Long> entry : results.entrySet()) {
			handler.HandleResult(String.format("%s : %s", entry.getKey(), entry.getValue()));
		}
	}
}
