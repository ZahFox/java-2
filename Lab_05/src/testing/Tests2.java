package testing;


import application.*;
import models.FileParser;
import models.Word;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.regex.Pattern;

import algorithms.*;
import models.WordComparator;

public class Tests2 {
	
	public static TreeSet<Word> data;
	private static ExecutionPool pPool;
	private static ExecutionPool cPool;
	private static final int MULTIPLIER = 2;
	private static final int MAX_THREADS = Runtime.getRuntime().availableProcessors() * MULTIPLIER;
	private static final ConcurrentLinkedQueue<Word> queue = new ConcurrentLinkedQueue<Word>();
	public static Boolean running = true;
	
	public static void main(String[] args) {

		
		
		pPool = new ExecutionPool(MAX_THREADS);
		cPool = new ExecutionPool(1);
		data = new TreeSet<Word>(new WordComparator());
		
		try(BufferedReader br = new BufferedReader(new FileReader("green_eggs_and_ham.txt"))) {
			String line;
			
			cPool.execute( () -> {
				while (Tests2.running) {
					if (!Tests2.queue.isEmpty()) {
						Word head = Tests2.queue.poll();
						System.out.println(head.getWord());
						if (head != null && !Tests2.data.contains(head))
							Tests2.data.add(head);
					}
				}
			});
			
			while ((line = br.readLine()) != null) {
				String ln = line;
				pPool.execute( () -> {
					for (String word : FileParser.WORD_REGEX.split(ln)) {
						if (!word.equals("")) {
							Tests2.queue.offer(new Word(word));
						}	
					}
				});
			}
			
			
			try {
				br.close();
				pPool.terminate();
				Tests2.running = false;
				cPool.terminate();
				Word[] words = data.toArray(new Word[data.size()]);
				System.out.println(words.length);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		
	}
		
}
