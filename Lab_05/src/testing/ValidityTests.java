package testing;

import static org.junit.jupiter.api.Assertions.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import algorithms.GenericMergeSort;
import application.AsyncHashMapWordParser;
import application.AsyncSkipListMapWordParser;
import application.SyncHashMapWordParser;
import application.TextFileAnalyzer;
import models.FileParser;
import models.Word;

class ValidityTests {
	
	private static final boolean VERBOSE_MODE = false;
	
	private static final File[] INPUT_FILES  = new File[] {
		new File("green_eggs_and_ham.txt"),
		new File("treasure_island.txt"),
		new File("shakespeare.txt")
	};
	private static final File[] OUTPUT_FILES = new File[] {
		new File("test_out_1.xml"),
		new File("test_out_2.xml"),
		new File("test_out_3.xml")
	};

	/**
	 * The expectedResults are generated before all tests
	 * by a known working synchronous implementation.
	 * This will assist in resolving an unexpected behavior
	 * from asynchronous implementations.
	 */
	private static Word[][] expectedResults;
	
	@BeforeAll
	public static void createExpectedResult() {
		assertEquals(INPUT_FILES.length, OUTPUT_FILES.length);
		expectedResults = generateResults(FileParser.Parser.SYNCHASHMAP, FileParser.Sorter.MERGESORT);
	}
	
	@Test
	void testAsyncHashMapMergeSort() {
		Word[][] testResults = generateResults(FileParser.Parser.ASYNCHASHMAP, FileParser.Sorter.MERGESORT);
		assertEquals(true, CompareResults("AsyncHashMapMergeSort", testResults));
	}
	
	@Test
	void testAsyncHashMapArrayParallelSort() {
		Word[][] testResults = generateResults(FileParser.Parser.ASYNCHASHMAP, FileParser.Sorter.ARRAYPARALLELSORT);
		assertEquals(true, CompareResults("AsyncHashMapArrayParallelSort", testResults));
	}
	
	@Test
	void testSyncHashMapMergeSort() {
		Word[][] testResults = generateResults(FileParser.Parser.SYNCHASHMAP, FileParser.Sorter.MERGESORT);
		assertEquals(true, CompareResults("SyncHashMapMergeSort", testResults));
	}
	
	@Test
	void testSyncHashMapArrayParallelSort() {
		Word[][] testResults = generateResults(FileParser.Parser.SYNCHASHMAP, FileParser.Sorter.ARRAYPARALLELSORT);
		assertEquals(true, CompareResults("SyncHashMapArrayParallelSort", testResults));
	}
	
	private static boolean CompareResults(String title, Word[][] results) {
		if (results.length != expectedResults.length) return false;
		int expectedMatches = 0, actualMatches = 0;
		
		for (int i = 0; i < expectedResults.length; i++ ) {
			expectedMatches += expectedResults[i].length;
			actualMatches += CompareWordArrays("SyncHashMapArrayParallelSort", expectedResults[i], results[i]);
		}
		
		System.out.printf("%s -- %d MATCHES OUT OF %d : %.2f%% ACCURACY %n",
			title, actualMatches, expectedMatches, ((double)actualMatches / (double)expectedMatches) * 100
		);
		return expectedMatches == actualMatches;
	}
	
	private static Word[][] generateResults(FileParser.Parser parser, FileParser.Sorter sorter) {
		try {
			Word[][] results = new Word[INPUT_FILES.length][];
			for (int i = 0; i < INPUT_FILES.length; i++) {	
				generateResult(results, i, parser, sorter);
			}
			return results;
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}	
	}
	
	private static void generateResult(Word[][] results, int index, FileParser.Parser parser, FileParser.Sorter sorter) {
		try {
			FileParser parseT = null;
			switch (parser) {
				case ASYNCHASHMAP:
					parseT = new AsyncHashMapWordParser();
					break;
				case SYNCHASHMAP:
					parseT = new SyncHashMapWordParser();
					break;
				case ASYNCSKIPLIST:
					parseT = new AsyncSkipListMapWordParser();
					break;
				case SYNCSKIPLIST:
					break;
			}
			final FileParser parseType = parseT;
			
			TextFileAnalyzer analyzer = new TextFileAnalyzer(
					INPUT_FILES[index], OUTPUT_FILES[index], parseType
			);
			analyzer.processInput(Optional.of( () -> {
				Word[] words = parseType.getData();
				
				switch (sorter) {
					case ARRAYPARALLELSORT:
						Arrays.parallelSort(words);
						analyzer.setWords(words);
						break;
					case MERGESORT:
						analyzer.setWords(GenericMergeSort.sort(parseType.getData()));
						break;
				}
			
				results[index] = words;	
			}));	
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static final int CompareWordArrays(String title, Word[] left, Word[] right) {
		int matchCount = 0;
		
		for (int i = 0; i < left.length; i++) {
			String name = left[i].getWord();
			int count = left[i].getCount();
			
			for (int j = 0; j < right.length; j++) {
				// Make sure words have the same count
				if (name.equals(right[j].getWord())) {
					if (count != right[j].getCount()) {
						printMismatch(title, name, count, right[j].getWord(), right[j].getCount());
					}
					else {
						matchCount++;
					}
				}
			}
		}
		
		return matchCount;
	}
	
	private static final void printMismatch(String title, String leftName, int i, String rightName, int j) {
		if (VERBOSE_MODE) {
			System.err.printf("%s -- MISMATCH: EXPECTED - %s: %d, GATHERED - %s: %d%n",
					title, leftName, i, rightName, j
			);
		}
	}

}
