package application;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

import algorithms.*;
import models.FileParser;
import models.Word;

public class AppDriver {
	
	private static final File INPUT_FILE  = new File("shakespeare.txt");
	private static final File OUTPUT_FILE = new File("test_out.xml");
	
	public static void main(String[] args) {
		ParseFile(new SyncHashMapWordParser(), FileParser.Sorter.MERGESORT);
	}
	
	public static final void ParseFile(FileParser parser, FileParser.Sorter sorter) {
		try {
			TextFileAnalyzer analyzer = new TextFileAnalyzer(
				INPUT_FILE, OUTPUT_FILE, parser
			);
			
			analyzer.processInput(Optional.of( () -> {
				Word[] words = parser.getData();
				words = SortWords(words, sorter);
				analyzer.setWords(words);
				try {
					analyzer.generateStatsFile();
				}
				catch (IOException e) {
					System.out.printf("There was an IO error while trying to write to %s%n", OUTPUT_FILE);
					System.err.println(e.getMessage());
				}
				catch (Exception e) {
					System.out.printf("There was an error while trying to write to %s%n", OUTPUT_FILE);
					System.err.println(e.getMessage());
				}
			}));
		}
		catch (IOException e) {
			System.out.printf("There was an IO error while trying to parse %s%n", INPUT_FILE);
			System.err.println(e.getMessage());
		}
		catch (InterruptedException e) {
			System.out.printf("There was an interrupted thread error while trying to parse %s%n", INPUT_FILE);
			System.err.println(e.getMessage());
		}
		catch (Exception e) {
			System.out.printf("There was an error while trying to parse %s%n", INPUT_FILE);
			System.err.println(e.getMessage());
		}
	}
	
	private static final Word[] SortWords(Word[] words, FileParser.Sorter sorter) {
		switch (sorter) {
		case ARRAYSORT:
			Arrays.sort(words);
			break;
		case ARRAYPARALLELSORT:
			Arrays.parallelSort(words);
			break;
		case MERGESORT:
			words = GenericMergeSort.sort(words);
			break;
		}
		return words;
	}

}
