package application;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Collection;
import java.util.concurrent.ConcurrentSkipListMap;

import models.AsyncFileParser;
import models.FileParser;
import models.Word;

public class AsyncSkipListMapWordParser extends AsyncFileParser {

	private ConcurrentSkipListMap<String, Word> data;

	public AsyncSkipListMapWordParser() {
		Init(   // Producer
				(file, consumer) -> {
					BufferedReader br = new BufferedReader(new FileReader(file));
					String line;
					while ((line = br.readLine()) != null) {
						consumer.consumeFile(line);
					}
					br.close();
				},
				// Consumer
				(String line) -> {
					Execute( () -> {
						for (String word : FileParser.WORD_REGEX.split(line)) {
							if (!word.equals("")) {
								word = word.toLowerCase();
								if (data.containsKey(word)) {
									data.computeIfPresent(word, (key, val) -> val.incrementUsage() );
								}
								else {
									data.computeIfAbsent(word, (key) -> new Word(key) );
								}
							}	
						}
					});
		});
		
		data = new ConcurrentSkipListMap<String, Word>();
	}


	@Override
	public Word[] getData() {
		Collection<Word> wordCollection = data.values();
		return wordCollection.toArray(new Word[wordCollection.size()]);
	}
	
}
