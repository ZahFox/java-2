package application;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Optional;

import models.FileParser;
import models.Word;

public class TextFileAnalyzer {

	private static final int EXPECTED_AVERAGE_WORD_LENGTH = 6;
	private final File inputFile, statsXMLFile;
	private Word[] words;
	private FileParser parser;
	
	public TextFileAnalyzer(File inputFile, File statsXMLFile, FileParser parser) throws IllegalArgumentException {
		if (!validFile(inputFile) || statsXMLFile == null || parser == null) {
			throw new IllegalArgumentException("TextFileAnalyzer was passed an invalid argument during its construction.");
		}
		else {
			this.inputFile = inputFile;
			this.statsXMLFile = statsXMLFile;
			this.parser = parser;
		}
	}
	
	public void processInput() throws IOException, InterruptedException {
		this.parser.parse(this.inputFile, Optional.empty());
	}
	
	public void processInput(Optional<Runnable> callback) throws IOException, InterruptedException {
		if (!callback.isPresent()) {
			this.parser.parse(this.inputFile, Optional.empty());
		}
		else {
			this.parser.parse(this.inputFile, callback);
		}
	}
	
	public void generateStatsFile() throws IOException {
		if (this.words != null && this.words.length > 0) {
			BufferedWriter bw = new BufferedWriter(new FileWriter(this.statsXMLFile));
			bw.write(getXML());
			bw.close();
		}
		else {
			throw new NullPointerException("TextFileAnalyzer is not allowed to create a stats file with no data.");
		}
			
	}
	
	public void setWords(Word[] words) throws NullPointerException {
		if (words != null) {
			this.words = words;
		}
		else {
			throw new NullPointerException("TextFileAnalyzer is not allowed to add an empty set of data.");
		}
	}

	private static boolean validFile(File file) {
		return (file != null && file.exists() && file.isFile() && file.canRead());
	}
	
	public String getXML() {
		StringBuilder sb = new StringBuilder(
				(this.words.length + 3) * EXPECTED_AVERAGE_WORD_LENGTH * 4
		);
		
		sb.append(String.format("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>%n<TEXT_ANALYZER>%n"));
		for (int i = this.words.length - 1; i > -1; i--) {
			sb.append(String.format("  <WORD>%n    <WORD_TEXT>%s</WORD_TEXT>%n    <USAGE_CNT>%d</USAGE_CNT>%n  </WORD>%n",
					this.words[i].getWord(), this.words[i].getCount()));
		}
		sb.append(String.format("</TEXT_ANALYZER>%n"));
		
		return sb.toString();
	}
	
}
