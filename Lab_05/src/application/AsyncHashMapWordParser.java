package application;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import interfaces.FileConsumer;
import interfaces.FileProducer;
import models.AsyncFileParser;
import models.FileParser;
import models.Word;

public class AsyncHashMapWordParser extends AsyncFileParser {

	private ConcurrentHashMap<String, Word> data;
	
	public AsyncHashMapWordParser() {
		Init(   // Producer
				(file, consumer) -> {
					BufferedReader br = new BufferedReader(new FileReader(file));
					String line;
					while ((line = br.readLine()) != null) {
						consumer.consumeFile(line);
					}
					br.close();
				},
				// Consumer
				(String line) -> {
					Execute( () -> {
						for (String word : FileParser.WORD_REGEX.split(line)) {
							if (!word.equals("")) {
								word = word.toLowerCase();
								if (data.containsKey(word)) {
									data.computeIfPresent(word, (key, val) -> val.incrementUsage() );
								}
								else {
									data.computeIfAbsent(word, (key) -> new Word(key) );
								}
							}	
						}
					});
		});
		
		data = new ConcurrentHashMap<String, Word>();
			
	}
	
	@Override
	public Word[] getData() {
		Collection<Word> wordCollection = data.values();
		return wordCollection.toArray(new Word[wordCollection.size()]);
	}
	
}
