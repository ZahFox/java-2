package application;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;

import models.FileParser;
import models.Word;

public class SyncHashMapWordParser extends FileParser {
	
	private HashMap<String, Word> data;
	
	public SyncHashMapWordParser() {
		Init(   // Producer
				(file, consumer) -> {
					BufferedReader br = new BufferedReader(new FileReader(file));
					String line;
					while ((line = br.readLine()) != null) {
						consumer.consumeFile(line);
					}
					br.close();
				},
				// Consumer
				(String line) -> {
					for (String word : FileParser.WORD_REGEX.split(line)) {
						if (!word.equals("")) {
							word = word.toLowerCase();
							Word value = data.get(word);
							if (value != null) {
								value.incrementUsage();
							}
							else {
								data.put(word, new Word(word));
							}
						}	
					}
				}
		);
		
		data = new HashMap<String, Word>();
	}
	
	public Word[] getData() {
		Collection<Word> wordCollection = data.values();
		return wordCollection.toArray(new Word[wordCollection.size()]);
	}
	
}
