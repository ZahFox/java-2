package application;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ExecutionPool {
	
	private ExecutorService executor;

	public static ExecutionPool getInstance(int threads) {
		return new ExecutionPool(threads);
	}
	
	public ExecutionPool(int threads) {
		this.executor = Executors.newFixedThreadPool(threads);
	}
	
	public void execute(Runnable task) {
		this.executor.execute(task);
	}
	
	public void terminate() throws InterruptedException {
		this.executor.shutdown();
		this.executor.awaitTermination(Long.MAX_VALUE, TimeUnit.MINUTES);
	}
	
}
