package application;

import java.util.Scanner;

public class DoDivision {

	private String numeratorInput;
	private String denominatorInput;
	
	private int numerator;
	private int denominator;
	private double quotient;
	
	public static void main(String[] args) {
		DoDivision doIt = new DoDivision();
		try {
			doIt.doNormalCase();
		}
		catch (DivideByZeroException e) {
			System.out.println(e.getMessage());
			try {
				doIt.giveSecondChance();
			}
			catch (DivideByZeroException e1) {
				System.out.println(e1.getMessage());
			}
			catch (NumberFormatException e2) {
				System.out.println("Sorry, this program requires the use of whole numbers.");
			}
			catch (Exception e3) {
				System.out.println("Error: The program has crashed unexpectedly.");
			}
		}
		catch (NumberFormatException e) {
			System.out.println("Sorry, you must enter a whole number. Please try again..");
			try {
				doIt.giveSecondChance();
			}
			catch (DivideByZeroException e1) {
				System.out.println(e1.getMessage());
			}
			catch (NumberFormatException e2) {
				System.out.println("Sorry, this program requires the use of whole numbers.");
			}
			catch (Exception e3) {
				System.out.println("Error: The program has crashed unexpectedly.");
			}
		}
		catch (Exception e) {
			System.out.println("Error: The program has crashed unexpectedly.");
		}
		finally {
			System.out.println("End of program.");
		}
	}
	
	public void doNormalCase() throws DivideByZeroException, NumberFormatException {
		System.out.println("Enter numerator:");
		Scanner keyboard = new Scanner(System.in);
		numeratorInput = keyboard.next();
		numerator = Integer.parseInt(numeratorInput);
		
		System.out.println("Enter denominator:");
		denominatorInput = keyboard.next();
        denominator = Integer.parseInt(denominatorInput);
		
		if (denominator == 0)
			throw new DivideByZeroException();
		
		quotient = numerator / (double)denominator;
		System.out.println(numerator + "/" + denominator + " = " + quotient);
		
	}
	
	public void giveSecondChance() throws DivideByZeroException, NumberFormatException {
			System.out.println("Enter numerator:");
	        Scanner keyboard = new Scanner(System.in);
	        numeratorInput = keyboard.next();
	        numerator = Integer.parseInt(numeratorInput);
	        
	        System.out.println("Enter denominator:");
	        System.out.println("Be sure the denominator is not zero.");
	        denominatorInput = keyboard.next();
	        denominator = Integer.parseInt(denominatorInput);
	        
			if (denominator == 0)
				throw new DivideByZeroException();
			
			quotient = numerator / (double)denominator;
	        System.out.println(numerator + "/" + denominator + " = " + quotient);
	}
	
}
