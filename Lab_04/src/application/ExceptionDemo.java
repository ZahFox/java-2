package application;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionDemo {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		
		try {
			System.out.println("Enter number of donuts:");
			String donutInput = keyboard.next();
			int donutCount = Integer.parseInt(donutInput);
			
			System.out.println("Enter number of glasses of milk:");
			String milkInput = keyboard.next();
			int milkCount = Integer.parseInt(milkInput);
			
			if (milkCount < 1)
				throw new Exception("Sorry, you must have atleast one glass of milk to participate..");
			
			double donutsPerGlass = donutCount / (double)milkCount;
			System.out.println(donutCount + " donuts.");
			System.out.println(milkCount + " glasses of milk.");
			System.out.println("You have " + donutsPerGlass + " donuts for each glass of milk.");
			
		}
		catch (NumberFormatException e) {
			System.out.println("Sorry, you must enter a whole number. Please try again..");
			
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("Go buy some milk.");
		}
		finally {
			keyboard.close();
			System.out.println("End of program.");
		}
	}

}
