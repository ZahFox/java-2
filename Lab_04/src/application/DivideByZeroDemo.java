package application;

import java.util.Scanner;

public class DivideByZeroDemo {

	private String numeratorInput;
	private String denominatorInput;
	
	private int numerator;
	private int denominator;
	private double quotient;
	
	public static void main(String[] args) {
		DivideByZeroDemo oneTime = new DivideByZeroDemo();
		oneTime.doIt();
	}
	
	public void doIt() {
		try {
			System.out.println("Enter numerator:");
			Scanner keyboard = new Scanner(System.in);
			numeratorInput = keyboard.next();
			numerator = Integer.parseInt(numeratorInput);
			System.out.println("Enter denominator:");
			denominatorInput = keyboard.next();
			denominator = Integer.parseInt(denominatorInput);
			keyboard.close();
			
			if (denominator == 0) {
				throw new DivideByZeroException();
			}
			
			quotient = numerator / (double)denominator;
			System.out.println(numerator + "/" + denominator +
								" = " + quotient);
		}
		catch (NumberFormatException e) {
			System.out.println("Sorry, you must enter a whole number. Please try again..");
			giveSecondChance();
		}
		catch (DivideByZeroException e) {
			System.out.println(e.getMessage());
			giveSecondChance();
		}
		catch (Exception e) {
			System.out.println("Error: The program has crashed unexpectedly, please try again.");
		}
		finally {
			System.out.println("End of program.");
		}
	}
	
	public void giveSecondChance() {
		try {
			System.out.println("Enter numerator:");
	        Scanner keyboard = new Scanner(System.in);
	        numeratorInput = keyboard.next();
	        numerator = Integer.parseInt(numeratorInput);
	        System.out.println("Enter denominator:");
	        System.out.println("Be sure the denominator is not zero.");
	        denominatorInput = keyboard.next();
	        denominator = Integer.parseInt(denominatorInput);
        	keyboard.close();
	        
	        if (denominator == 0) {
	        	System.out.println("I cannot do division by zero.");
	        	System.out.println("Since I cannot do what you want,");
	        	System.out.println("the program will now end.");
	        	System.exit(0);
	        }
	        quotient = ((double)numerator) / denominator;
	        System.out.println(numerator + "/" + denominator + " = " + quotient);
		}
		catch (NumberFormatException e) {
			System.out.println("Sorry, you must enter a whole number. Please try again..");
			giveSecondChance();
		}
		catch (Exception e) {
			System.out.println("Error: The program has crashed unexpectedly, please try again.");
		}
	}

}
