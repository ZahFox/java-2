package ui;

import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import org.junit.experimental.theories.Theories;

import algorithms.BinarySearch;
import algorithms.MergeSort;
import application.ExecutionTask;
import application.ExecutionTimer;
import application.RandomNumberArray;

import java.awt.Font;
import java.awt.event.ActionEvent;

public final class RecursionUIX extends RecursionUI {

	private int[] sortedNumbersList;
	private double lastSortTime;
	private double lastSearchTime;
	private int lastIntCount;
	private int lastIntStart;
	private int lastIntEnd;
	private int lastIntSearch;
	public static RecursionUIX currentUI;
	
	private static final String NUMBER_OF_INTS_TITLE = "Number of Integers";
	private static final String INTEGER_RANGE_FROM_TITLE = "Integer Starting Range";
	private static final String INTEGER_RANGE_TO_TITLE = "Integer Ending Range";
	private static final String SEARCH_VALUE_TITLE = "Search Value";
	
	
	public RecursionUIX() {
		super();
		currentUI = this;
		
		btnGenerate.addActionListener( (args) -> {
			new GenerateNumberListTask(this).run();
		});
		
		btnSearch.addActionListener( (args) -> {
			new SearchNumberTask(this).run();
		});
		
		frmRecursionLab.setVisible(true);
	}
	
	private final boolean numberTextFieldsAreValid(HashMap<String, JTextField> textFields) {
		int number = -1;
		String title = "", text = "";
		JTextField textField;
		
		for (Entry<String, JTextField> field : textFields.entrySet()) {
			title = field.getKey();
			textField = field.getValue();
			text = textField.getText().trim();
			textField.setText(text);
			
			if (text.equals("")) {
				JOptionPane.showMessageDialog(frmRecursionLab,
					String.format("The %s is required!", title));
				return false;
			}
			
			try {
				number = Integer.parseInt(text);
			}
			catch (Exception e) {
				JOptionPane.showMessageDialog(frmRecursionLab,
					String.format("The %s must be an integer!", title));
				return false;
			}
			
			if (!setTextFieldNumberValue(title, number)) {
				JOptionPane.showMessageDialog(frmRecursionLab,
					String.format("The %s must be greater than zero!", title));
				return false;
			}
		}
		
		return true;
	}
	
	private boolean setTextFieldNumberValue(String title, int value) {
		if (value > 0) {
			switch (title) {
			case NUMBER_OF_INTS_TITLE:
				this.lastIntCount = value;
				break;
			case INTEGER_RANGE_FROM_TITLE:
				this.lastIntStart = value;
				break;
			case INTEGER_RANGE_TO_TITLE:
				this.lastIntEnd = value;
				break;
			case SEARCH_VALUE_TITLE:
				if (sortedNumbersList != null && sortedNumbersList.length > 0) {
					this.lastIntSearch = value;
				}
				break;
			}
			return true;
		}
		else {
			return false;
		}
	}
	
	private static final void generateNumberList(RecursionUIX context) throws InterruptedException {
		context.clearLastSortTime(true);
		
		double time = sortNumberList(context, RandomNumberArray.generate(
						Integer.parseInt(context.txtNumberOfInts.getText()),
						Integer.parseInt(context.txtIntegerRangeFrom.getText()),
						Integer.parseInt(context.txtIntegerRangeTo.getText())));
		
		if (time > -1) context.setLastSortTime(time);
		else context.clearLastSortTime(true);
	}
	
	private static final int searchNumberList(RecursionUIX context, int value) throws InterruptedException {
		context.clearLastSearchTime(true);
		final int[] searchIndex = new int[1];
		int[] list = context.getSortedNumberList();
		
		double time = ExecutionTimer.getElapsedTime(new Thread( () -> {
			searchIndex[0] = BinarySearch.search(list, value, 0, list.length - 1);
		}), ExecutionTimer.TimeFormat.MILLISECOND);
		
		if (searchIndex[0] > -1) {
			context.setLastSearchTime(time);
		}
		else {
			context.clearLastSearchTime(false);
		}

		return searchIndex[0];
	}
	
	private class GenerateNumberListTask extends SwingWorker<Void, Void> {
		private JButton button;
		private RecursionUIX context;
		
		public GenerateNumberListTask(RecursionUIX context) {
			super();
			this.context = context;
		}
		
		@Override
		protected Void doInBackground() throws Exception {
			btnSearch.setEnabled(false);
			btnGenerate.setEnabled(false);
			
			Thread thread = new Thread( () -> {
				if (numberTextFieldsAreValid( new HashMap<String, JTextField>(){
					private static final long serialVersionUID = 1L;
				{   put(NUMBER_OF_INTS_TITLE, txtNumberOfInts);
					put(INTEGER_RANGE_FROM_TITLE, txtIntegerRangeFrom);
					put(INTEGER_RANGE_TO_TITLE, txtIntegerRangeTo);
				}})) {
					if (context.getLastIntEnd() > context.getLastIntStart()) {
						try {
							RecursionUIX.generateNumberList(currentUI);
						}
						catch (Exception e) {
							JOptionPane.showMessageDialog(frmRecursionLab, 
							    "There was an unexpected error while generating the number list.");
						}
					}
					else {
						JOptionPane.showMessageDialog(frmRecursionLab,
								String.format("The %s must be less than the %s",
									RecursionUIX.INTEGER_RANGE_FROM_TITLE, RecursionUIX.INTEGER_RANGE_TO_TITLE));
					}

				}
				
				btnSearch.setEnabled(true);
				btnGenerate.setEnabled(true);
			});
			
			thread.start();
			return null;
		}	
	}

	private class SearchNumberTask extends SwingWorker<Void, Void> {
		private JButton button;
		private RecursionUIX context;
		
		public SearchNumberTask(RecursionUIX context) {
			super();
			this.context = context;
		}
		
		@Override
		protected Void doInBackground() throws Exception {
			btnSearch.setEnabled(false);
			btnGenerate.setEnabled(false);
			
			Thread thread = new Thread( () -> {
				if (numberTextFieldsAreValid( new HashMap<String, JTextField>(){
					private static final long serialVersionUID = 2L;
				{   put(SEARCH_VALUE_TITLE, txtSearchForNumber); }})) {
					
					try {
						if (sortedNumbersList != null && sortedNumbersList.length > 0) {
							int index = searchNumberList(context, lastIntSearch);
							if (index > -1) {
								lstNumberList.ensureIndexIsVisible(index);
								lstNumberList.setSelectedIndex(index);
							}
							else {
								JOptionPane.showMessageDialog(frmRecursionLab, 
								    String.format("The value: %d was not found in the number list!", lastIntSearch));
							}
						}
						else {
							JOptionPane.showMessageDialog(frmRecursionLab, 
							   "There currently are no values to search from!");
						}
	
					} catch (Exception e) {
						JOptionPane.showMessageDialog(frmRecursionLab, 
						    "There was an unexpected error while searching the numbers list.");
					}
					
				}
				
				btnSearch.setEnabled(true);
				btnGenerate.setEnabled(true);
			});
			
			thread.start();
			return null;
		}	
	}
	
	private static final double sortNumberList(RecursionUIX context, int[] numberList) throws InterruptedException {
		double elapsedTime = -1;
		try {
			final int[][] numbers = new int[1][];
			elapsedTime = ExecutionTimer.getElapsedTime(new Thread( () -> {
				numbers[0] = MergeSort.sort(numberList);
			}), ExecutionTimer.TimeFormat.MILLISECOND);
			context.setSortedNumberList(numbers[0]);
		}
		catch (Exception e) {
			elapsedTime = -1;
		}
		return elapsedTime;
	}
	
	private final void setSortedNumberList(int[] sortedNumbers) {
		if (sortedNumbers.length > 0) {
			this.sortedNumbersList = sortedNumbers;
			this.lstNumberList.setEnabled(false);
			JList<Integer> newList = new JList<Integer>();
			newList.setListData(Arrays.stream(sortedNumbers).boxed().toArray(Integer[]::new));
			newList.setFont(new Font("Tahoma", Font.BOLD, 12));
			this.lstNumberList = newList;
			scrNumberList.setViewportView(newList);
		}
	}
	
	private final void clearLastSortTime(boolean loading) {
		if (loading) this.txtSortTime.setText("Loading...");
		else this.txtSortTime.setText("");
	}
	
	private final void setLastSortTime(double lastSortTime) {
		if (lastSortTime > 0) {
			this.lastSortTime = lastSortTime;
			this.txtSortTime.setText(String.format("%.4fms", lastSortTime));
		}
	}
	
	private final void clearLastSearchTime(boolean loading) {
		if (loading) this.txtSearchTime.setText("Loading...");
		else this.txtSearchTime.setText("");
	}
	
	public final void setLastSearchTime(double lastSearchTime) {
		if (lastSearchTime > 0) {
			this.lastSearchTime = lastSearchTime;
			this.txtSearchTime.setText(String.format("%.4fms", lastSearchTime));
		}
	}
	
	public final int getLastIntStart() {
		return this.lastIntStart;
	}
	
	public final int getLastIntEnd() {
		return this.lastIntEnd;
	}
	
	public final int[] getSortedNumberList() {
		return this.sortedNumbersList;
	}
	
}
