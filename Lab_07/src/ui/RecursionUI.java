package ui;

import javax.swing.JFrame;
import javax.swing.JButton;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JList;

import java.util.Arrays;

import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JPanel;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;
import java.awt.Color;

public class RecursionUI {

	protected JFrame frmRecursionLab;
	protected JTextField txtNumberOfInts;
	protected JList<Integer> lstNumberList;
	protected JLabel lblIntegerRangeFrom;
	protected JTextField txtIntegerRangeFrom;
	protected JLabel lblIntegerRangeTo;
	protected JTextField txtIntegerRangeTo;
	protected JLabel lblNumberOutput;
	protected JTextField txtSearchForNumber;
	protected JLabel lblSortTime;
	protected JLabel lblSearchTime;
	protected JTextField txtSearchTime;
	protected JLabel txtStats;
	protected JTextField txtSortTime;
	protected JButton btnGenerate;
	protected JButton btnSearch;
	protected JScrollPane scrNumberList;

	/**
	 * Create the application.
	 * @wbp.parser.entryPoint
	 */
	public RecursionUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private final void initialize() {
		frmRecursionLab = new JFrame();
		frmRecursionLab.setAlwaysOnTop(true);
		frmRecursionLab.getContentPane().setBackground(Color.WHITE);
		frmRecursionLab.setBackground(Color.WHITE);
		frmRecursionLab.setTitle("Recursion Lab");
		frmRecursionLab.setBounds(100, 100, 377, 520);
		frmRecursionLab.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmRecursionLab.getContentPane().setLayout(null);
		
		JLabel lblNumberOfInts = new JLabel("# of Ints:");
		lblNumberOfInts.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNumberOfInts.setBounds(26, 26, 65, 25);
		frmRecursionLab.getContentPane().add(lblNumberOfInts);
		
		txtNumberOfInts = new JTextField();
		txtNumberOfInts.setFont(new Font("Tahoma", Font.BOLD, 12));
		txtNumberOfInts.setBounds(101, 26, 230, 25);
		frmRecursionLab.getContentPane().add(txtNumberOfInts);
		txtNumberOfInts.setColumns(10);
		
		btnGenerate = new JButton("Generate Numbers");
		btnGenerate.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		btnGenerate.setBounds(26, 96, 305, 25);
		frmRecursionLab.getContentPane().add(btnGenerate);
		
		scrNumberList = new JScrollPane();
		scrNumberList.setBounds(26, 239, 140, 200);
		frmRecursionLab.getContentPane().add(scrNumberList);
		
		lstNumberList = new JList<Integer>();
		lstNumberList.setFont(new Font("Tahoma", Font.BOLD, 12));
		scrNumberList.setViewportView(lstNumberList);
		
		lblIntegerRangeFrom = new JLabel("Int Range:");
		lblIntegerRangeFrom.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblIntegerRangeFrom.setBounds(26, 61, 70, 25);
		frmRecursionLab.getContentPane().add(lblIntegerRangeFrom);
		
		txtIntegerRangeFrom = new JTextField();
		txtIntegerRangeFrom.setFont(new Font("Tahoma", Font.BOLD, 12));
		txtIntegerRangeFrom.setBounds(101, 61, 100, 25);
		frmRecursionLab.getContentPane().add(txtIntegerRangeFrom);
		txtIntegerRangeFrom.setColumns(10);
		
		lblIntegerRangeTo = new JLabel("to");
		lblIntegerRangeTo.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblIntegerRangeTo.setHorizontalAlignment(SwingConstants.RIGHT);
		lblIntegerRangeTo.setBounds(201, 61, 20, 25);
		frmRecursionLab.getContentPane().add(lblIntegerRangeTo);
		
		txtIntegerRangeTo = new JTextField();
		txtIntegerRangeTo.setFont(new Font("Tahoma", Font.BOLD, 12));
		txtIntegerRangeTo.setColumns(10);
		txtIntegerRangeTo.setBounds(231, 61, 100, 25);
		frmRecursionLab.getContentPane().add(txtIntegerRangeTo);
		
		lblNumberOutput = new JLabel("# Output:");
		lblNumberOutput.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNumberOutput.setBounds(26, 214, 100, 25);
		frmRecursionLab.getContentPane().add(lblNumberOutput);
		
		JLabel lblSearchForNumber = new JLabel("Search for #:");
		lblSearchForNumber.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblSearchForNumber.setBounds(26, 174, 85, 25);
		frmRecursionLab.getContentPane().add(lblSearchForNumber);
		
		txtSearchForNumber = new JTextField();
		txtSearchForNumber.setFont(new Font("Tahoma", Font.BOLD, 12));
		txtSearchForNumber.setBounds(121, 174, 100, 25);
		frmRecursionLab.getContentPane().add(txtSearchForNumber);
		txtSearchForNumber.setColumns(10);
		
		btnSearch = new JButton("Search");
		btnSearch.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnSearch.setBounds(231, 174, 100, 25);
		frmRecursionLab.getContentPane().add(btnSearch);
		
		JPanel pnlStats = new JPanel();
		pnlStats.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		pnlStats.setBounds(191, 239, 140, 200);
		frmRecursionLab.getContentPane().add(pnlStats);
		pnlStats.setLayout(null);
		
		lblSortTime = new JLabel("Sort Time:");
		lblSortTime.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblSortTime.setBounds(10, 21, 100, 25);
		pnlStats.add(lblSortTime);
		
		lblSearchTime = new JLabel("Search Time:");
		lblSearchTime.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblSearchTime.setBounds(10, 81, 100, 25);
		pnlStats.add(lblSearchTime);
		
		txtSearchTime = new JTextField();
		txtSearchTime.setFont(new Font("Tahoma", Font.BOLD, 12));
		txtSearchTime.setEditable(false);
		txtSearchTime.setColumns(10);
		txtSearchTime.setBounds(10, 111, 100, 25);
		pnlStats.add(txtSearchTime);
		
		txtSortTime = new JTextField();
		txtSortTime.setFont(new Font("Tahoma", Font.BOLD, 12));
		txtSortTime.setEditable(false);
		txtSortTime.setColumns(10);
		txtSortTime.setBounds(10, 51, 100, 25);
		pnlStats.add(txtSortTime);
		
		txtStats = new JLabel("Stats:");
		txtStats.setFont(new Font("Tahoma", Font.BOLD, 12));
		txtStats.setBounds(191, 214, 100, 25);
		frmRecursionLab.getContentPane().add(txtStats);
	}
}
