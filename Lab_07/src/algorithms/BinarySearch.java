package algorithms;

public final class BinarySearch {

	private BinarySearch() {}
	
	public static final int search(int[] array, int target, int lower, int upper) {
		if (lower > upper) return -1;
		
		int mid = (lower + upper) / 2;
		if (array[mid] == target) return mid;
		
		if (target > array[mid]) return search(array, target, mid + 1, upper);
		else return search(array, target, lower, mid - 1);
	}
	
}
