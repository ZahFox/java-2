package algorithms;

public final class MergeSort {

	private MergeSort() {}
	
	public static final int[] sort(int[] array) {
		if (array.length == 1) return array;
		
		Object[] splitArray = split(array);
		int[] left = (int[])splitArray[0];
		int[] right = (int[])splitArray[1];
		
		left = sort(left);
		right = sort(right);
		
		return merge(left, right);
	}
	
	public static final Object[] split(int[] array) {
		int[] left = null;
		int[] right = null;
		
		int mid = array.length / 2;
		
		if (array.length % 2 == 0) {
			left = new int[mid];
			right = new int[mid];
		}
		else {
			left = new int[mid];
			right = new int[mid + 1];
		}
		
		for (int i = 0; i < left.length; i++) {
			left[i] = array[i];
		}
		
		for (int i = 0; i < right.length; i++) {
			right[i] = array[i + mid];
		}
		
		Object[] o = new Object[2];
		o[0] = left;
		o[1] = right;
		
		return o;
		
	}
	
	public static final int[] merge(int[] left, int[] right) {
		int[] array = new int[left.length + right.length];
		int l = 0, r = 0, i = 0;
		
		while (i < array.length && l < left.length && r < right.length) {
			int nextLeft = left[l];
			int nextRight = right[r];
			
			if (nextLeft < nextRight) {
				array[i] = nextLeft;
				l++;
			}
			else {
				array[i] = nextRight;
				r++;
			}
			
			i++;
		}
			
		while (l < left.length) {
			array[i++] = left[l++];
		}
		
		while (r < right.length) {
			array[i++] = right[r++];
		}
		
		return array;
	}
	
}
