package testing;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import algorithms.BinarySearch;
import algorithms.MergeSort;
import application.ExecutionTimer;
import application.RandomNumberArray;


class TestNumberOperations {

	@Test
	void testNumberGeneration() {
		final int size = 9001, min = 33, max = 343;
		int[] numbers = RandomNumberArray.generate(size, min, max);
		assertEquals(size, numbers.length);
		for (int i = 0; i < size; i++) {
			assertTrue(numbers[i] >= min && numbers[i] <= max);
		}
	}
	
	@Test
	void testMergeSort() {
		int[] sortedNumbers = {1, 2, 3, 5, 6, 7, 20, 33, 42, 55, 67, 67, 901, 902};
		int[] unsortedNumbers = {20, 33, 3, 2, 1, 7, 5, 6, 67, 55, 901, 902, 67, 42};
		assertEquals(sortedNumbers.length, unsortedNumbers.length);
		unsortedNumbers = MergeSort.sort(unsortedNumbers);	
		assertArrayEquals(sortedNumbers, unsortedNumbers);
	}
	
	@Test
	void testBinarySearch() {
		int[] testArray1 = {1, 2, 3, 4, 5};
		int[] testArray2 = {1};
		assertEquals(0, BinarySearch.search(testArray1, 1, 0, testArray1.length - 1));
		assertEquals(1, BinarySearch.search(testArray1, 2, 0, testArray1.length - 1));
		assertEquals(2, BinarySearch.search(testArray1, 3, 0, testArray1.length - 1));
		assertEquals(3, BinarySearch.search(testArray1, 4, 0, testArray1.length - 1));
		assertEquals(4, BinarySearch.search(testArray1, 5, 0, testArray1.length - 1));
		assertEquals(-1, BinarySearch.search(testArray1, 6, 0, testArray1.length - 1));
		assertEquals(0, BinarySearch.search(testArray2, 1, 0, testArray2.length - 1));
		assertEquals(-1, BinarySearch.search(testArray2, 2, 0, testArray2.length - 1));
		assertEquals(-1, BinarySearch.search(testArray2, 3, 0, testArray2.length - 1));
		assertEquals(-1, BinarySearch.search(testArray2, 4, 0, testArray2.length - 1));
		assertEquals(-1, BinarySearch.search(testArray2, 5, 0, testArray2.length - 1));
	}
	
	@Test
	void testAllOperations() {
		try {
			final double timeElapsed = ExecutionTimer.getElapsedTime(new Thread(() -> {
				final int size = 9001, min = 33, max = 343;
				int[] numbers = RandomNumberArray.generate(size, min, max);
				int[] javaSortedNumbers = Arrays.copyOf(numbers, numbers.length);
				Arrays.sort(javaSortedNumbers);
				assertFalse(Arrays.equals(numbers, javaSortedNumbers));
				numbers = MergeSort.sort(numbers);
				assertArrayEquals(javaSortedNumbers, numbers);
				for (int i = 0; i < size; i++) {
					assertEquals(numbers[i], numbers[BinarySearch.search(numbers, numbers[i], 0, numbers.length - 1)]);
				}
			}), ExecutionTimer.TimeFormat.MILLISECOND);
			System.out.printf("The testAllOperations test took approximately %fms%n", timeElapsed);
		}
		catch (InterruptedException e) {
			System.err.println("Error: The testAllOperations timer thread was interrupted!");
		}
		catch (Exception e) {
			System.err.println("Error: An unexpected error occured during the testAllOperations test!");
		}
		
	}

}
