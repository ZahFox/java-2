package application;

public final class ExecutionTimer {

	private ExecutionTimer() {}
	
	public enum TimeFormat {
		MINUTE, SECOND, MILLISECOND, MICROSECOND, NANOSECOND
	}
	
	public static final double getElapsedTime(Thread task, TimeFormat format) throws InterruptedException {
		long beginTime = System.nanoTime();
		
		task.run();
		task.join();
		
		long endTime = System.nanoTime();
		return formatResult((double)(endTime - beginTime), format);
	}
	
	private static final double formatResult(double elapsedTime, TimeFormat format) {
		switch (format) {
		case MICROSECOND:
			elapsedTime = elapsedTime / 1000D;
			break;
		case MILLISECOND:
			elapsedTime = elapsedTime / 1000000D;
			break;
		case SECOND:
			elapsedTime = elapsedTime / 1000000000D;
			break;
		case MINUTE:
			elapsedTime = elapsedTime / 60000000000D;
			break;
		}
		
		return elapsedTime;
	}
	
}
