package application;

import java.util.concurrent.ThreadLocalRandom;

public final class RandomNumberArray {

	private RandomNumberArray() {}
	
	public static final int[] generate(int size, int min, int max) {
		int[] numbers = new int[size];
		
		for (int i = 0; i < size; i++) {
			// This method's max is exclusive, so adding 1 makes it inclusive
			numbers[i] = ThreadLocalRandom.current().nextInt(min, max + 1);
		}
		
		return numbers;
	}
	
}
