package application;

import java.awt.EventQueue;

import ui.RecursionUIX;

public class AppDriver {
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(() -> {
			try {
				new RecursionUIX();
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}
	
}
