# Java 2 - Western Technical College #

In this repository you will find most my coursework from Java 2 a 2018 Spring course at Western Technical College in La Crosse, Wisconsin. This course has the following description:
"The goal as programmers, is to create reliable, efficient, and sustainable solutions to problems. Java 2
introduces powerful design concepts and algorithms that programmers utilize to solve challenging problems
congruent with these goals. The course starts with a brisk review of select topics from Java I and proceeds to
more advanced design centric concepts. Using the Java programming language, Java 2 focuses on the more
complex aspects of programming, design, data structures, algorithms, I/O, performance, and etc."

### Labs ###

* Lab 01 - [Java I Topics Review Lab](./Lab_01/src)
* Lab 02 - [Java I Topics Review Lab Part 2](./Lab_02/src)
* Lab 03 - [Inheritance, Polymorphism, and Interfaces](./Lab_03/src)
* Lab 04 - [Exception Handling](./Lab_04/src)
* Lab 05 - [File I/O – Text File Processing](./Lab_05/src)


### Course Competencies ###

1. Review data types, objects, classes, and control structures.
2. Examine object inheritance, polymorphism, and interfaces.
3. Utilize exception handling.
4. Explore file input and output.
5. Apply recursive concepts and techniques.
6. Utilize abstract data types.
7. Examine searching and sorting algorithms.
8. Explore Big O notation and performance concepts
9. Explore building graphical user interfaces.
