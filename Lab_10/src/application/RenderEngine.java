package application;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import mazegui.Maze;
import mazegui.MazeGUI;

public final class RenderEngine {

	public static final int HEIGHT = 1080;
	public static final int WIDTH = 1920;
	
	private static RenderEngine instance = null;
	private static MazePanel panel;
	
	private int cellCountRoot = 25;
	private int mazeCountRoot;
	private int mazeBoxPadding;
	
	private RenderEngine(int mazeCountRoot) {
		this.mazeCountRoot = mazeCountRoot;
		JFrame frame = new JFrame("Maze Solver Extreme");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(WIDTH, HEIGHT);
		panel = new MazePanel(frame);
		
		JPanel borderPanel = new JPanel();

		borderPanel.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2, true));
		borderPanel.setBackground(Color.WHITE);
		
		JScrollPane mazePane = new JScrollPane(panel);
		JScrollPane borderPane = new JScrollPane(borderPanel);
		frame.add(borderPane);
		frame.add(mazePane);
	}
	
	public static final RenderEngine init(int rootMazeCount) {
		if (instance == null) {
			instance = new RenderEngine(rootMazeCount);
			return instance;
		}
		else {
			return instance;
		}
	}
	
	public static final void draw() {
		panel.repaint();
	}
	
	public MazeGUI[] render() {
		MazeGUI[] guiAccess = new MazeGUI[this.mazeCountRoot * this.mazeCountRoot];
		Size mazeContainerSize = getMazeDimensions();
		Size infoContainerSize = getInfoDimensions();
		int centerAlignmentOffset = (mazeContainerSize.width - mazeContainerSize.height) / 2;
		
		// 10% Padding
		mazeBoxPadding = (int)(((double)mazeContainerSize.width / this.mazeCountRoot) / 10);
		Maze.CELL_WIDTH = (30 / this.mazeCountRoot);
		Maze.DOT_SIZE = (15 / this.mazeCountRoot);
		Maze.DOT_MARGIN = (6 / this.mazeCountRoot);
		
		int row = 0, column = 0;
		for (int i = 0; i < (this.mazeCountRoot * this.mazeCountRoot); i++, column++) {
			if (i % (mazeCountRoot) == 0 && i != 0) row += 1;
			
			int xOffset = centerAlignmentOffset + (mazeBoxPadding / 2) + ( (mazeContainerSize.height / this.mazeCountRoot) * column );
			int yOffset = (mazeBoxPadding / 2) + ( (mazeContainerSize.height / this.mazeCountRoot) * row );
			
			guiAccess[i] = new MazeGUI(panel, cellCountRoot, xOffset, yOffset);
			
			if (column == (this.mazeCountRoot - 1)) column = -1;
		}
		
		panel.show();
		return guiAccess;
	}
	
	private static final Size getMazeDimensions() {
		int width = WIDTH;
		int height = (HEIGHT / 6) * 5;
		return new Size(width, height);
	}
	
	private static final Size getInfoDimensions() {
		int width = WIDTH;
		int height = (HEIGHT / 6);
		return new Size(width, height);
	}
	
	public final class MazePanel extends JPanel {
		private ArrayList<Maze> mazes;
		private JFrame frame;

		public MazePanel(JFrame frame) {
			this.frame = frame;
			this.mazes = new ArrayList<Maze>();
		}
		
		public void add(Maze maze) {
			mazes.add(maze);
		}
		
		public void show() {
			this.frame.setVisible(true);
		}

		public void paintComponent(Graphics page) {
			super.paintComponent(page);
			
			setBackground(Color.BLACK);
			
			for (Maze maze : mazes) {
				maze.draw(page);
			}
		}
	}

}


final class Size {
	public int width;
	public int height;
	
	public Size(int width, int height) {
		this.width = width;
		this.height = height;
	}
}