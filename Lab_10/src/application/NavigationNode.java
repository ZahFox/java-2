package application;

import mazegui.Maze.Cell;

public class NavigationNode implements Comparable<NavigationNode> {

	private int[] walls;
	private int index;
	private boolean visited;
	
	public NavigationNode(Cell cell, int index) {
		this.walls = cell.getWalls();
		this.index = index;
	}
	
	public int getIndex() {
		return this.index;
	}
	
	public int getNorth() {
		return this.walls[0];
	}

	public int getSouth() {
		return this.walls[1];
	}


	public int getEast() {
		return this.walls[2];
	}

	public int getWest() {
		return this.walls[3];
	}
	
	public boolean getVisited() {
		return this.visited;
	}
	
	public void setVisited() {
		this.visited = true;
	}
	
	

	@Override
	public int compareTo(NavigationNode node) {
		return Integer.compare(this.index, node.index);
	}
	
}
