package application;

import java.util.Stack;

import mazegui.MazeGUI;
import mazegui.Maze.Cell;
import mazegui.Maze.PathAccessor;

public class PathFinder {

	private boolean isFinished;
	private NavigationNode[] nodes;
	private PathAccessor pathAccessor;
	private int size;
	private int root;
	
	public static final PathFinder create(MazeGUI maze) {
		PathFinder newFinder = new PathFinder();
		newFinder.isFinished = false;
		newFinder.size = maze.getSize();
		newFinder.root = maze.getRoot();
		newFinder.nodes = new NavigationNode[newFinder.size];
		newFinder.pathAccessor = maze.getPathAccessor();
		
		Cell[] cells = maze.getCells();
		
		for (int i = 0; i < cells.length; i++) {
			newFinder.nodes[i] = new NavigationNode(cells[i], i);
		}
		
		return newFinder;
	}
	
	public boolean isFinished() {
		return this.isFinished;
	}
	
	public void begin() {
		Thread newSearch = new Thread( () -> {
			try {
				startSearch(this);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}	
		});
		
		newSearch.start();
	}
	
	private static final void startSearch(PathFinder finder) throws InterruptedException {
		NavigationNode current = finder.nodes[0];
		final NavigationNode finish = finder.nodes[finder.size - 1];
		
		int currentIndex = 0;
		
		Stack<NavigationNode> pathStack = new Stack<NavigationNode>();
		current.setVisited();
		currentIndex = current.getIndex();
		
		finder.pathAccessor.setPath(currentIndex, true);
		
		while (current != finish) {	
			
			pathStack.push(current);
			
			if (current.getEast() == finder.size && !finder.nodes[currentIndex + 1].getVisited()) {
				NavigationNode newNode = finder.nodes[currentIndex + 1];
					newNode.setVisited();
					current = newNode;
					currentIndex = newNode.getIndex();
					finder.pathAccessor.setPath(currentIndex, true);
			}
			else if (current.getSouth() == finder.size && !finder.nodes[currentIndex + finder.root].getVisited()) {
				NavigationNode newNode = finder.nodes[currentIndex + finder.root];
					newNode.setVisited();
					current = newNode;
					currentIndex = newNode.getIndex();
					finder.pathAccessor.setPath(currentIndex, true);
			}
			else if (current.getWest() == finder.size && currentIndex != 0 && !finder.nodes[currentIndex - 1].getVisited()) {
				NavigationNode newNode = finder.nodes[currentIndex - 1];
					newNode.setVisited();
					current = newNode;
					currentIndex = newNode.getIndex();
					finder.pathAccessor.setPath(currentIndex, true);
			}	
			else if (current.getNorth() == finder.size && !finder.nodes[currentIndex - finder.root].getVisited()) {
				NavigationNode newNode = finder.nodes[currentIndex - finder.root];
					newNode.setVisited();
					current = newNode;
					currentIndex = newNode.getIndex();
					finder.pathAccessor.setPath(currentIndex, true);
			}
			else {
				finder.pathAccessor.setPath(currentIndex, false);
				pathStack.pop();
				current = pathStack.pop();
				currentIndex = current.getIndex();
			}
			
			Thread.sleep(100);
		}
		
		finder.pathAccessor.setPath(currentIndex, true);
		finder.isFinished = true;
	}
	
}
