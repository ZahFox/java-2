package application;

import mazegui.MazeGUI;

import java.io.IOException;

public class AppDriver {

	public static final int ROOT_MAZE_COUNT = 3;
	
	public static void main(String[] args) throws IOException, InterruptedException {
		boolean isRunning = true;
		
		RenderEngine renderer = RenderEngine.init(ROOT_MAZE_COUNT);
		MazeGUI[] guiAccess = renderer.render();
		PathFinder[] pathFinders = new PathFinder[ROOT_MAZE_COUNT * ROOT_MAZE_COUNT];
		
		for (int i = 0; i < guiAccess.length; i++) {
			pathFinders[i] = PathFinder.create(guiAccess[i]); 
		}

		for (int i = 0; i < pathFinders.length; i++) {
			pathFinders[i].begin();
		}
		
		while (isRunning) {
			renderer.draw();
		}
	}
	
}
