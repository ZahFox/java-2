package mazegui;

import application.RenderEngine;
import mazegui.Maze.Cell;
import mazegui.Maze.PathAccessor;

public final class MazeGUI {
	
	private int cellCountRoot;
	private int xOffset;
	private int yOffset;
	private Maze maze;
	
	public MazeGUI(RenderEngine.MazePanel panel, int cellCountRoot,int xOffset, int yOffset) {
		this.cellCountRoot = cellCountRoot;
		this.maze = new Maze(cellCountRoot, xOffset, yOffset);
		panel.add(maze);
		this.xOffset = xOffset;
		this.yOffset = yOffset;
	}
	
	public Cell[] getCells() { return this.maze.getCells(); }
	public PathAccessor getPathAccessor() { return this.maze.getPathAccessor(); }
	public int getSize() { return this.cellCountRoot * this.cellCountRoot; }
	public int getRoot() { return this.cellCountRoot; }
	
}
