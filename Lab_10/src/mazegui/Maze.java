/*
 * Maze.java 
 * Author: Irene Alvarado 
 * Maze object that creates a maze using a
 * disjoint set representing cells and running a modified version of Kruskal's
 * algorithm to remove walls. In the end, the maze walls are drawn as well as a
 * unique path in red dots.
 */
package mazegui;

import java.awt.*;
import java.util.Random;

import utilities.Generator;

public class Maze
{
	private enum Zone { FAR, MID, CLOSE };
	
	public static int CELL_WIDTH = 10; // maze square size  -- 10 for 3x3
	public static int DOT_SIZE = 5; // size of maze solution dot  -- 5 for 3x3
	public static int DOT_MARGIN = 2; // space between wall and dot -- 2 for 2x2
	
	public Color wallColor;
	
	private int leftMargin;
	private int rightMargin;
	
	// Zone Ranges
	private int farMin;
	private int farMax;
	private int midMin;
	private int midMax;
	private int closeMin;
	private int closeMax;
	
	private int lastPathIndex;
	
	private int N;
	private Cell[] cells; // array containing all the cells in the maze
	private boolean[] path; // array representing the unique path solution

	public Maze(int n, int leftMargin, int rightMargin)
	{
		this.N = n;
		this.leftMargin = leftMargin;
		this.rightMargin = rightMargin;
		
		setZoneValues();
		
		this.wallColor = Generator.RandomColor();
		
		cells = new Cell[N * N]; // creates array of Cells
		
		for (int i = 0; i < N * N; i++) // initializes array with Cell objects
		{
			cells[i] = new Cell();
		}
	
		if(N > 0)
		{
			makeWalls(); // updates wall information inside each Cell object
			clearWalls(); // destoys wall until a maze is formed
		
			path = new boolean[N * N];
			createPath();
		}
	}
	
	private void setZoneValues() {
		farMin = 0;
		farMax = (N / 2);
		midMin = farMax + 1;
		closeMin = ( ((N / 2) - 1) + (N - 1) );
		midMax = closeMin - 1;
		closeMax = ( (N - 1) + (N - 1) );
	}
	
	private Color getZoneColor(Point coordinates) {
		int sum = coordinates.x + coordinates.y;
		if (sum >= farMin && sum < midMin) {
			return Color.RED;
		}
		else if (sum >= midMin && sum < closeMin) {
			return Color.WHITE;
		}
		else {
			return Color.GREEN;
		}
	}
	
	private Point getCoordinates(int index) {
		int x = index / N;
		int y = index % N;
		return new Point(x, y);
	}
	
	public Cell[] getCells() {
		return this.cells;
	}
	
	public PathAccessor getPathAccessor() {
		return new PathAccessor(this.path);
	}
	
	public class PathAccessor {
		private boolean[] path;
		
		public PathAccessor(boolean[] path) {
			this.path = path;
		}
		
		public void setPath(int index, boolean value) {
			path[index] = value;
			lastPathIndex = index;
		}
	}

	public class Cell {
		int[] walls; // array representing north, south, east, west walls
		int visitedBy; // for running first breath search, saves the cell that
		// visited this cell
		
		public Cell()
		{
			walls = new int[4];
			visitedBy = -1;
		}
		
		public int[] getWalls() {
			return this.walls;
		}
	}
	
	final int NORTH = 0 ;
	final int SOUTH = 1 ;
	final int EAST = 2 ;
	final int WEST = 3 ;

	public void makeWalls() // fills wall information in Cells, -1 represents a
	// border wall
	{
		for (int i = 0; i < N * N; i++) // set north,south,east,west walls
		{
			cells[i].walls[NORTH] = i - N;
			cells[i].walls[SOUTH] = i + N;
			cells[i].walls[EAST] = i + 1;
			cells[i].walls[WEST] = i - 1;
		}
		
		for (int i = 0; i < N; i++)
		{
			cells[i].walls[NORTH] = -1; // set in border north cells, north wall to -1
			cells[N * N - i - 1].walls[SOUTH] = -1; // set in border south cells, south
			// wall to -1
		}
		for (int i = 0; i < N * N; i += N)
		{
			cells[N * N - i - 1].walls[EAST] = -1; // set in border east cells, east
			// wall to -1
			cells[i].walls[WEST] = -1; // set in border west cells, west wall to -1
		}
	}

	public void clearWalls() // destroys walls with a modified version of
	// Kruskal's algorithm
	{
		int NumElements = N * N;
		
		DisjSets ds = new DisjSets(NumElements); // creates a disjoint set to
		// represent cells
		for (int k = 0; k < N * N; k++)
		{
			ds.find(k); // adds each cell to a single set
		}
		
		Random generator = new Random();
		while (ds.allConnected() == false) // while not all the elements in the
		// set are connected
		{
			int cell1 = generator.nextInt(N * N); // pick a random cell
			int wall = generator.nextInt(4);
			
			int cell2 = cells[cell1].walls[wall]; // pick a second random cell
			
			if (cell2 != -1 && cell2 != N * N) // if there exists a wall between
			// these two cells
			{
				if (ds.find(cell1) != ds.find(cell2)) // if cells do not belong to
				// the same set
				{
					cells[cell1].walls[wall] = N * N; // destroy the wall between
					// these two cells. N*N will
					// represent no wall
					
					if (wall == NORTH || wall == EAST)
					{
						cells[cell2].walls[wall + 1] = N * N;
					}
					if (wall == SOUTH || wall == WEST)
					{
						cells[cell2].walls[wall - 1] = N * N;
					}
					
					ds.union(ds.find(cell1), ds.find(cell2)); // make a union of the
					// set of these two cells, through which a path has just been
					// created
				}
			}
		}
	}

	public void createPath() // finds a path in the maze
	{
		cells[0].walls[WEST] = N * N; // destroys west wall on top left cell
		cells[N * N - 1].walls[EAST] = N * N; // destroys east wall on bottom right		
	}

	public void draw(Graphics g) // draws a maze and its solution
	{
		g.setColor(this.wallColor);
		
		for (int i = 0; i < N; i++)
		{
			int count = i;
			for (int j = 0; j < N; j++)
			{
				if (j != 0)
				{
					count += N;
				}
				
				if (cells[count].walls[NORTH] != N * N) // if there exists a wall to the
				// north
				{
					g.drawLine((i * CELL_WIDTH + leftMargin), (j * CELL_WIDTH + rightMargin),
						((i + 1) * CELL_WIDTH + leftMargin), (j * CELL_WIDTH + rightMargin));
				}
				
				if (cells[count].walls[SOUTH] != N * N) // if there exists a wall to the
				// south
				{
					g.drawLine(i * CELL_WIDTH + leftMargin, (j + 1) * CELL_WIDTH
						+ rightMargin, (i + 1) * CELL_WIDTH + leftMargin, (j + 1) * CELL_WIDTH
						+ rightMargin);
				}
				
				if (cells[count].walls[EAST] != N * N) // if there exists a wall to the
				// east
				{
					g.drawLine((i + 1) * CELL_WIDTH + leftMargin, j * CELL_WIDTH
						+ rightMargin, (i + 1) * CELL_WIDTH + leftMargin, (j + 1) * CELL_WIDTH
						+ rightMargin);
				}
				
				if (cells[count].walls[WEST] != N * N) // if there exists a wall to the
				// west
				{
					g.drawLine(i * CELL_WIDTH + leftMargin, j * CELL_WIDTH + rightMargin, i
						* CELL_WIDTH + leftMargin, (j + 1) * CELL_WIDTH + rightMargin);
				}
			}
		}
		
		g.setColor(getZoneColor(getCoordinates(this.lastPathIndex))); // changes color to draw the dots
		
		for (int i = 0; i < N; i++)
		{
			int count = i;
			for (int j = 0; j < N; j++)
			{
				if (j != 0)
				{
					count += N;
				}
				
				if (path[count] == true) // if cell is part of the path
				{
					g.fillOval(i * CELL_WIDTH + leftMargin + DOT_MARGIN, j * CELL_WIDTH
						+ rightMargin + DOT_MARGIN, DOT_SIZE, DOT_SIZE); // paint a red
					// circle in the
					// cell

				}
			}
		}
	}

	public Dimension windowSize() // returns the ideal size of the window (for
	// JScrollPanes)
	{
		return new Dimension(N * CELL_WIDTH + leftMargin * 2, N * CELL_WIDTH + leftMargin
			* 2);
	}
}
