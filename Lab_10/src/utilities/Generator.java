package utilities;

import java.awt.Color;
import java.util.Random;

public final class Generator {

	public static final Color RandomColor() {
		Random rand = new Random();
		return new Color(rand.nextFloat(), rand.nextFloat(), rand.nextFloat());
	}
	
}
