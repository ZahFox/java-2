package testing;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.jupiter.api.Test;

import application.CustomLinkedList;
import java.util.concurrent.ThreadLocalRandom;

class TestCustomLinkedList {

	@Test
	void basicTest() {
		CustomLinkedList<String> customLinkedList = new CustomLinkedList<String>();
		String[] expectedValues = {"Hello", "World!", "My", "Name", "Is", "Will"};
		int massCount = 9001;
		
		// Test Add to End of List
		customLinkedList.add("Hello");
		customLinkedList.add("World!");
		customLinkedList.add("My");
		customLinkedList.add("Name");
		customLinkedList.add("Is");
		customLinkedList.add("Will");
		
		// Test Getting the Size
		assertEquals(6, customLinkedList.size());
		
		// Test Getting Value by Index
		for (int i = 0; i < customLinkedList.size(); i++) {
			assertEquals(expectedValues[i], customLinkedList.get(i));
		}
		int size = customLinkedList.size();
		
		for (int i = 0; i < massCount; i++) {
			int randomNum = ThreadLocalRandom.current().nextInt(0, customLinkedList.size() + 1);
			customLinkedList.add(Integer.toString(randomNum));
			assertEquals(Integer.toString(randomNum), customLinkedList.get(size + i));
			assertEquals(true, customLinkedList.contains(Integer.toString(randomNum)));
		}
		
		assertEquals(massCount + size, customLinkedList.size());
		customLinkedList.clear();
		assertEquals(0, customLinkedList.size());
		size = customLinkedList.size();
		
		for (int i = 0; i < massCount; i++) {
			int randomNum = ThreadLocalRandom.current().nextInt(0, customLinkedList.size() + 1);
			customLinkedList.add(Integer.toString(randomNum));
			assertEquals(Integer.toString(randomNum), customLinkedList.get(size + i));
			assertEquals(true, customLinkedList.contains(Integer.toString(randomNum)));
		}
		
		assertEquals(massCount + size, customLinkedList.size());
		customLinkedList.clear();
		assertEquals(0, customLinkedList.size());
	}
	
	@Test
	void addAtIndexTest() {
		CustomLinkedList<String> customLinkedList1 = new CustomLinkedList<String>();
		customLinkedList1.clear();
		customLinkedList1.clear();
		customLinkedList1.clear();
		customLinkedList1.add(0, "HELLO");
		customLinkedList1.add(0, "WORLD");
		assertEquals("WORLD", customLinkedList1.get(0));
		assertEquals("HELLO", customLinkedList1.get(1));
		customLinkedList1.add("TOO");
		customLinkedList1.add("MUCH");
		customLinkedList1.add("FUN");
		assertEquals("TOO", customLinkedList1.get(2));
		assertEquals("MUCH", customLinkedList1.get(3));
		assertEquals("FUN", customLinkedList1.get(4));
		customLinkedList1.add("WORLD");
		customLinkedList1.add("WORLD");
		customLinkedList1.add("WORLD");
		customLinkedList1.add("WORLD");
		assertEquals("TOO", customLinkedList1.get(2));
		assertEquals("MUCH", customLinkedList1.get(3));
		assertEquals("FUN", customLinkedList1.get(4));
		customLinkedList1.add(0, "TACO");
		assertEquals("TOO", customLinkedList1.get(3));
		assertEquals("MUCH", customLinkedList1.get(4));
		assertEquals("FUN", customLinkedList1.get(5));
		customLinkedList1.add(1, "TACO");
		assertEquals("TOO", customLinkedList1.get(4));
		assertEquals("MUCH", customLinkedList1.get(5));
		assertEquals("FUN", customLinkedList1.get(6));
		customLinkedList1.add(6, "TACO-1");
		customLinkedList1.add(7, "TACO-2");
		customLinkedList1.add(8, "TACO-3");
		assertEquals("TACO-1", customLinkedList1.get(6));
		assertEquals("TACO-2", customLinkedList1.get(7));
		assertEquals("TACO-3", customLinkedList1.get(8));
		assertEquals("TOO", customLinkedList1.get(4));
		assertEquals("MUCH", customLinkedList1.get(5));
		assertEquals("FUN", customLinkedList1.get(9));
		assertEquals(false, customLinkedList1.contains(null));
		customLinkedList1.add(null);
		assertEquals(true, customLinkedList1.contains(null));
	}
	
	@Test
	void toArrayTest() {
		int massCount = 9001;
		CustomLinkedList<String> customLinkedList = new CustomLinkedList<String>();
		
		for (int i = 0; i < massCount; i++) {
			int randomNum = ThreadLocalRandom.current().nextInt(0, customLinkedList.size() + 1);
			customLinkedList.add(Integer.toString(randomNum));		
		}
		Object[] array = customLinkedList.toArray();
		for (int i = 0; i < massCount; i++) {
			assertEquals(customLinkedList.get(i), (String) array[i]);		
		}
	}
	
	@Test
	void removeTest() {
		int massCount = 9001;
		CustomLinkedList<String> customLinkedList = new CustomLinkedList<String>();
		ArrayList<String> testData = new ArrayList<String>();
		
		for (int i = 0; i < massCount; i++) {
			int randomNum = ThreadLocalRandom.current().nextInt(0, customLinkedList.size() + 1);
			customLinkedList.add(Integer.toString(randomNum));
			assertEquals(true, customLinkedList.remove(Integer.toString(randomNum)));
		}

		for (int i = 0; i < massCount; i++) {
			int randomNum = ThreadLocalRandom.current().nextInt(0, customLinkedList.size() + 1);
			testData.add(Integer.toString(randomNum));
			customLinkedList.add(Integer.toString(randomNum));
		}
		for (String val : testData) {
			assertEquals(true, customLinkedList.remove(val));
		}
		assertEquals(0, customLinkedList.size());
		
		CustomLinkedList<String> customLinkedList2 = new CustomLinkedList<String>();
		
		for (int i = 0; i < massCount; i++) {
			int randomNum = ThreadLocalRandom.current().nextInt(0, 9001);
			customLinkedList2.add(Integer.toString(randomNum));
			assertEquals(Integer.toString(randomNum), customLinkedList2.remove(0));
		}
		
		assertEquals(0, customLinkedList2.size());
		
		for (int i = 0; i < massCount; i++) {
			int randomNum = ThreadLocalRandom.current().nextInt(0, 9001);
			customLinkedList2.add(Integer.toString(randomNum));
		}
		
		assertEquals(massCount, customLinkedList2.size());
		
		for (int i = 0; i < massCount; i++) {
			int randomNum = ThreadLocalRandom.current().nextInt(0, 9001);
			customLinkedList2.add(Integer.toString(randomNum));
			if (i % 2 == 0) {
				assertEquals(true, customLinkedList2.remove(Integer.toString(randomNum)));
				assertEquals(massCount, customLinkedList2.size());
			}
			else {
				assertEquals(Integer.toString(randomNum), customLinkedList2.remove(massCount));
				assertEquals(massCount, customLinkedList2.size());
			}
		}
		
		customLinkedList.add(null);
		assertEquals(true, customLinkedList.contains(null));
		assertEquals(true, customLinkedList.remove(null));
		assertEquals(false, customLinkedList.contains(null));
		customLinkedList.add(null);
		assertEquals(true, customLinkedList.contains(null));
		int index = customLinkedList.indexOf(null);
		assertEquals(null, customLinkedList.remove(index));
		assertEquals(false, customLinkedList.contains(null));
		customLinkedList.add(null);
		assertEquals(true, customLinkedList.contains(null));
		index = customLinkedList.lastIndexOf(null);
		assertEquals(null, customLinkedList.remove(index));
		assertEquals(false, customLinkedList.contains(null));
	}
	
	@Test
	void setTest() {
		int massCount = 9001;
		CustomLinkedList<String> customLinkedList1 = new CustomLinkedList<String>();
		CustomLinkedList<String> customLinkedList2 = new CustomLinkedList<String>();
		
		for (int i = 0; i < massCount; i++) {
			int randomNum = ThreadLocalRandom.current().nextInt(0, 9001);
			customLinkedList1.add(Integer.toString(randomNum));
			customLinkedList2.add(Integer.toString(randomNum));
			assertEquals(customLinkedList1.get(i), customLinkedList2.get(i));
		}
		
		for (int i = 0; i < massCount; i++) {
			int randomNum = ThreadLocalRandom.current().nextInt(0, 9001);
			String old = customLinkedList1.set(i, Integer.toString(randomNum));
			assertEquals(old, customLinkedList2.get(i));
		}
		
		assertEquals(customLinkedList1.size(), customLinkedList2.size());
	}
	
	@Test
	void testIndexOf() {
		int massCount = 9001;
		CustomLinkedList<String> customLinkedList = new CustomLinkedList<String>();
		ArrayList<String> testData = new ArrayList<String>();
		
		for (int i = 0; i < massCount; i++) {
			customLinkedList.add(Integer.toString(i));
			testData.add(Integer.toString(i));
		}
		for (int i = 0; i < massCount; i++) {
			assertEquals(testData.indexOf(Integer.toString(i)), customLinkedList.indexOf(Integer.toString(i)));
		}
		
		CustomLinkedList<String> customLinkedList2 = new CustomLinkedList<String>();
		for (int i = 0; i < massCount; i++) {
			customLinkedList2.add(Integer.toString(i));
		}
		for (int i = -1 * massCount; i < 2 * massCount; i++) {
			if (i >= 0 && i < massCount) assertEquals(i, customLinkedList2.indexOf(Integer.toString(i)));
			else assertEquals(-1, customLinkedList2.indexOf(Integer.toString(i)));
		}
	}
	
	@Test
	void testLastIndexOf() {
		int massCount = 9001;
		CustomLinkedList<String> customLinkedList = new CustomLinkedList<String>();
		ArrayList<String> testData = new ArrayList<String>();
		
		for (int i = 0; i < massCount; i++) {
			int randomNum = ThreadLocalRandom.current().nextInt(0, 9001);
			customLinkedList.add(Integer.toString(randomNum));
			testData.add(Integer.toString(randomNum));
		}
		for (int i = 0; i < massCount; i++) {
			assertEquals(testData.lastIndexOf(Integer.toString(i)), customLinkedList.lastIndexOf(Integer.toString(i)));
		}
		
		CustomLinkedList<String> customLinkedList2 = new CustomLinkedList<String>();
		for (int i = 0; i < massCount; i++) {
			customLinkedList2.add(Integer.toString(i));
		}
		for (int i = -1 * massCount; i < 2 * massCount; i++) {
			if (i >= 0 && i < massCount) assertEquals(i, customLinkedList2.lastIndexOf(Integer.toString(i)));
			else assertEquals(-1, customLinkedList2.lastIndexOf(Integer.toString(i)));
		}
	}
	
	@Test
	void testExceptions() {
		final CustomLinkedList<String> errorList1 = new CustomLinkedList<String>();
		TestForException(() -> errorList1.add(-1, "HELLO!"), "IndexOutOfBoundsException");
		TestForException(() -> errorList1.set(-1, "HELLO!"), "IndexOutOfBoundsException");
		TestForException(() -> errorList1.remove(-1), "IndexOutOfBoundsException");
		TestForException(() -> errorList1.get(-1), "IndexOutOfBoundsException");
		TestForException(() -> errorList1.add(9001, "HELLO!"), "IndexOutOfBoundsException");
		TestForException(() -> errorList1.set(9001, "HELLO!"), "IndexOutOfBoundsException");
		TestForException(() -> errorList1.remove(9001), "IndexOutOfBoundsException");
		TestForException(() -> errorList1.get(9001), "IndexOutOfBoundsException");
	}
	
	@Test
	void testExtreme() {
		ArrayList<String> trusted = new ArrayList<String>();
		trusted.add("BEGIN");
		CustomLinkedList<String> untrusted = new CustomLinkedList<String>();
		untrusted.add("BEGIN");
		int massCount = 50000;
		int stringSize = 20;
		
		for (int i = 0; i < massCount; i++) {
			int randomIndex = ThreadLocalRandom.current().nextInt(0, 13);
			int size = trusted.size();
			int randomInt = ThreadLocalRandom.current().nextInt(0, size > 0 ? size : 1);
			String randomString = getSaltString(stringSize);
			ExtremeTestingSwitch(
					randomIndex, randomInt, randomString,
					trusted, untrusted, trusted.size(), untrusted.size());
		}
	}
	
	private static final <T> void ExtremeTestingSwitch(
			int index,
			int optionalInt,
			T optionalType,
			List<T> list1,
			List<T> list2,
			int list1Size,
			int list2Size
		) {
		try {
			
			switch (index) {
			// size()
			case 0:
				assertEquals(list1.size(), list2.size());
				break;
			// isEmpty()
			case 1:
				assertEquals(list1.isEmpty(), list2.isEmpty());
				break;
			// contains(Object o)
			case 2:
				assertEquals(list1.contains(optionalType), list2.contains(optionalType));
				break;
			// toArray()
			case 3:
				assertArrayEquals(list1.toArray(), list2.toArray());
				break;
			// add(Object o)
			case 4:
				assertEquals(list1.add(optionalType), list2.add(optionalType));
				break;
			// remove(Object o)
			case 5:
				assertEquals(list1.remove(optionalType), list2.remove(optionalType));
				break;
			// clear()
			case 6:
				list1.clear(); list2.clear();
				break;
			// get(int index)
			case 7:
				assertEquals(list1.get(optionalInt), list2.get(optionalInt));
				break;
			// set(int index, Object o)
			case 8:
				assertEquals(list1.set(optionalInt, optionalType), list2.set(optionalInt, optionalType));
				break;
			// add(int index, Object o)
			case 9:
				list1.add(optionalInt, optionalType); list2.add(optionalInt, optionalType);
				break;
			// remove(int index)
			case 10:
				assertEquals(list1.remove(optionalInt), list2.remove(optionalInt));
				break;
			// indexOf(Object o)
			case 11:
				assertEquals(list1.indexOf(optionalType), list2.indexOf(optionalType));
				break;
			// lastIndexOf(Object o)
			case 12:
				assertEquals(list1.lastIndexOf(optionalType), list2.lastIndexOf(optionalType));
				break;
			default:
				throw new IndexOutOfBoundsException(String.format("%d is not an acceptable index for extreme testing!", index));
			}
			
		}
		catch (IndexOutOfBoundsException e) {
			System.out.printf("INDEX: %d, OPTIONALINT: %d, LIST1_LEN: %d, LIST2_LEN: %d%n",
					index, optionalInt, list1Size, list2Size);
		}
	}
	
	private static final void TestForException (Runnable test, String expectedExceptionType) {
		try { test.run(); assertEquals(false, true); }
		catch (Exception e) { assertEquals(expectedExceptionType, e.getClass().getSimpleName()); }
	}
	
	private static final String getSaltString(int length) {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < length) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }

}
