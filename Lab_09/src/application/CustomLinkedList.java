package application;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class CustomLinkedList<T> implements List<T> {
	
	private Node<T> head;
	private Node<T> tail;
	private int size;
	
	public CustomLinkedList() {
		this.size = 0;
		this.head = new Node<T>(null);
		this.tail = new Node<T>(null);
		this.head.next = this.tail;
	}

	@Override
	public boolean add(T data) {
		Node<T> newNode = new Node<T>(data);
		this.tail.next = newNode;
		this.tail = newNode; 
		this.size++;
		return true;
	}

	@Override
	public void add(int index, T data) {
		if (index >= 0 && index <= this.size) {
			if (this.size == 0) this.add(data);
			else {
				Node<T> newNode = new Node<T>(data);
				Node<T> prevNode = this.head.next;
				Node<T> currentNode = this.head.next.next;
				
				for (int i = 0; i < index; i++) {
					prevNode  = currentNode;
					currentNode = currentNode.next;
				}
				
				prevNode.next = newNode;
				newNode.next = currentNode;
				this.size++;
			}
		}
		else {
			throw new IndexOutOfBoundsException();
		}
	}

	@Override
	public boolean addAll(Collection<? extends T> arg0) {
		throw new UnsupportedOperationException("Not Implemented");
	}

	@Override
	public boolean addAll(int arg0, Collection<? extends T> arg1) {
		throw new UnsupportedOperationException("Not Implemented");
	}

	@Override
	public void clear() {
		for(Node<T> current = this.head.next.next; current != null; ) {
			Node<T> next = current.next;
			current.data = null;
			current.next = null;
			current = next;
		}
		
		this.tail = head.next;
		this.size = 0;
	}

	@Override
	public boolean contains(Object t) {
		if (!this.isEmpty()) {
			T target = (T) t;
			Node<T> currentNode = this.head.next.next;
			
			while(currentNode != null) {
				if (t == null ? currentNode.data == null : currentNode.data.equals(target)) return true;
				currentNode = currentNode.next;
			}
			
			return false;
		}
		return false;
	}

	@Override
	public boolean containsAll(Collection<?> arg0) {
		throw new UnsupportedOperationException("Not Implemented");
	}

	@Override
	public T get(int index) {
		if (index >= 0 && index < this.size) {
			Node<T> currentNode = this.head.next.next;
			
			for (int i = 0; i < index; i++) {
				currentNode = currentNode.next;
			}
			
			return currentNode.data;
		}
		else {
			throw new IndexOutOfBoundsException();
		}
	}

	@Override
	public int indexOf(Object t) {
		if (!this.isEmpty()) {
			T target = (T) t;
			int i = 0;
			Node<T> prevNode = this.head.next;
			Node<T> currentNode = this.head.next.next;
			
			while (currentNode != null) {
				if (t == null ? currentNode.data == null : currentNode.data.equals(target)) {
					return i;
				}
				prevNode = currentNode;
				currentNode = currentNode.next;
				i++;
			}
		}
		return -1;
	}

	@Override
	public boolean isEmpty() {
		return this.size <= 0;
	}

	@Override
	public Iterator<T> iterator() {
		throw new UnsupportedOperationException("Not Implemented");
	}

	@Override
	public int lastIndexOf(Object t) {
		if (!this.isEmpty()) {
			T target = (T) t;
			int i = 0, j = -1;
			Node<T> prevNode = this.head.next;
			Node<T> currentNode = this.head.next.next;
			
			while (currentNode != null) {
				if (t == null ? currentNode.data == null : currentNode.data.equals(target)) {
					j = i;
				}
				prevNode = currentNode;
				currentNode = currentNode.next;
				i++;
			}
			
			return j;
		}
		return -1;
	}

	@Override
	public ListIterator<T> listIterator() {
		throw new UnsupportedOperationException("Not Implemented");
	}

	@Override
	public ListIterator<T> listIterator(int arg0) {
		throw new UnsupportedOperationException("Not Implemented");
	}

	@Override
	public boolean remove(Object t) {
		if (!this.isEmpty()) {
			T target = (T) t;
			Node<T> prevNode = this.head.next;
			Node<T> currentNode = this.head.next.next;
			
			while (currentNode != null) {
				if (t == null ? currentNode.data == null : currentNode.data.equals(target)) {
					if (currentNode == this.tail) this.tail = prevNode;
					prevNode.next = currentNode.next;
					currentNode.data = null;
					currentNode = null;
					this.size--;
					return true;
				}
				prevNode = currentNode;
				currentNode = currentNode.next;
			}
		}
		return false;
	}

	@Override
	public T remove(int index) {
		if (index >= 0 && index < this.size) {

			Node<T> prevNode = this.head.next;
			Node<T> currentNode = this.head.next.next;
			
			for (int i = 0; i < index; i++) {
				prevNode  = currentNode;
				currentNode = currentNode.next;
			}
			
			if (currentNode == this.tail) this.tail = prevNode;
			T oldData = currentNode.data;
			
			prevNode.next = currentNode.next;
			currentNode = null;
			this.size--;
				
			return oldData;
		}
		else {
			throw new IndexOutOfBoundsException();
		}
	}

	@Override
	public boolean removeAll(Collection<?> arg0) {
		throw new UnsupportedOperationException("Not Implemented");
	}

	@Override
	public boolean retainAll(Collection<?> arg0) {
		throw new UnsupportedOperationException("Not Implemented");
	}

	@Override
	public T set(int index, T data) {
		if (index >= 0 && index < this.size) {

			Node<T> currentNode = this.head.next.next;
			
			for (int i = 0; i < index; i++) {
				currentNode = currentNode.next;
			}
			
			T oldData = currentNode.data;
			currentNode.data = data;
			
			return oldData;
		}
		else {
			throw new IndexOutOfBoundsException();
		}
	}

	@Override
	public int size() {
		return this.size;
	}

	@Override
	public List<T> subList(int arg0, int arg1) {
		throw new UnsupportedOperationException("Not Implemented");
	}

	@Override
	public Object[] toArray() {
		Object[] result = new Object[this.size];
		Node<T> currentNode = this.head.next.next;
		int index = 0;
		
		while (currentNode != null) {
			result[index++] = currentNode.data;
			currentNode = currentNode.next;
		}
		
		return result;
	}

	@Override
	public <T> T[] toArray(T[] arg0) {
		throw new UnsupportedOperationException("Not Implemented");
	}
	
	// Private Inner Classes
	private class Node<T> {
		private T data;
		private Node<T> next;
		
		public Node(T data) {
			this.data = data;
			this.next = null;
		}
	}

	
}
