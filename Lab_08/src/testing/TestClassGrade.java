package testing;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import models.ClassGrade;

class TestClassGrade {

	@Test
	void testConstructorGettersAndSetters() {
		String expectedException1 = ClassGrade.INVALID_CREDITS,
			   expectedException2 = ClassGrade.INVALID_GRADE,
			   expectedException3 = ClassGrade.INVALID_COURSE_ID;
		
		TestException(expectedException1, () -> new ClassGrade("TEST", -9001, 200));
		TestException(expectedException1, () -> new ClassGrade("TEST", 0, -500));
		TestException(expectedException2, () -> new ClassGrade("TEST", 1, 0));
		TestException(expectedException2, () -> new ClassGrade("TEST", 1, 0.01));
		TestException(expectedException2, () -> new ClassGrade("TEST", 1, 0.5));
		TestException(expectedException2, () -> new ClassGrade("TEST", 1, 1.5));
		TestException(expectedException2, () -> new ClassGrade("TEST", 1, 2.51));
		TestException(expectedException2, () -> new ClassGrade("TEST", 1, 200));
		TestException(expectedException3, () -> new ClassGrade("", 1, 200));
		
		assertEquals(true, TestNoException(() -> new ClassGrade("TEST", 1, 0.00D)));
		assertEquals(true, TestNoException(() -> new ClassGrade("TEST", 1, 1.00D)));
		assertEquals(true, TestNoException(() -> new ClassGrade("TEST", 1, 2.00D)));
		assertEquals(true, TestNoException(() -> new ClassGrade("TEST", 1, 2.50D)));
		assertEquals(true, TestNoException(() -> new ClassGrade("TEST", 1, 3.00D)));
		assertEquals(true, TestNoException(() -> new ClassGrade("TEST", 1, 3.50D)));
		assertEquals(true, TestNoException(() -> new ClassGrade("TEST", 1, 4.00D)));
		
		ClassGrade testClassGrade = new ClassGrade("TEST", 3, 3.50D);
		double testGradePoints1 = 10.50D;
		double testGradePoints2 = 36004D;
		
		TestException(expectedException1, () -> testClassGrade.setCredits(-9001));
		TestException(expectedException1, () -> testClassGrade.setCredits(-1));
		TestException(expectedException1, () -> testClassGrade.setCredits(0));
		TestException(expectedException2, () -> testClassGrade.setGrade(0.01));
		TestException(expectedException2, () -> testClassGrade.setGrade(0.5D));
		TestException(expectedException2, () -> testClassGrade.setGrade(1.5));
		TestException(expectedException2, () -> testClassGrade.setGrade(2.51));
		TestException(expectedException2, () -> testClassGrade.setGrade(200));
		TestException(expectedException3, () -> testClassGrade.setClassId(""));
		
		assertEquals("TEST", testClassGrade.getClassId());
		assertEquals(3, testClassGrade.getCredits());
		assertEquals(3.50D, testClassGrade.getGrade());
		assertEquals(testGradePoints1, testClassGrade.getGradePoints());
		
		testClassGrade.setClassId("TEST-V2");
		testClassGrade.setCredits(9001);
		testClassGrade.setGrade(4.00D);
		
		assertEquals("TEST-V2", testClassGrade.getClassId());
		assertEquals(9001, testClassGrade.getCredits());
		assertEquals(4.00D, testClassGrade.getGrade());
		assertEquals(testGradePoints2, testClassGrade.getGradePoints());
	}
	
	@Test
	void testToString() {
		String expectedString1 = "Class: Java I Credits: 3 Grade: 4.00 Grade Points: 12.00",
			   expectedString2 = "Class: Java II Credits: 4 Grade: 3.50 Grade Points: 14.00",
		       expectedString3 = "Class: Database I Credits: 3 Grade: 3.00 Grade Points: 9.00",
		       expectedString4 = "Class: English Credits: 3 Grade: 2.00 Grade Points: 6.00";
		
		ClassGrade testClassGrade1 = new ClassGrade("Java I", 3, 4.00D),
				   testClassGrade2 = new ClassGrade("Java II", 4, 3.50D),
				   testClassGrade3 = new ClassGrade("Database I", 3, 3.00D),
				   testClassGrade4 = new ClassGrade("English", 3, 2.00D);
		
		assertEquals(expectedString1, testClassGrade1.toString());
		assertEquals(expectedString2, testClassGrade2.toString());
		assertEquals(expectedString3, testClassGrade3.toString());
		assertEquals(expectedString4, testClassGrade4.toString());
	}
	
	private static final void TestException(String expected, Runnable task) {
		try {
			task.run();
		}
		catch(IllegalArgumentException e) {
			assertEquals(expected, e.getMessage());
		}
	}
	
	private static final boolean TestNoException(Runnable task) {
		try {
			task.run();
			return true;
		}
		catch(IllegalArgumentException e) {
			return false;
		}
	}

}
