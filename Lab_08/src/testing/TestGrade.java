package testing;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import models.Grade;

class TestGrade {

	@Test
	void testConstructorGettersAndSetters() {
		String expectedException1 = Grade.AVAILABLE_POINTS_TOO_SMALL,
			   expectedException2 = Grade.EARNED_POINTS_TOO_SMALL,
			   expectedException3 = Grade.EARNED_POINTS_TOO_BIG;
		
		TestException(expectedException1, () -> new Grade(20, 0));
		TestException(expectedException2, () -> new Grade(-1, 20));
		TestException(expectedException3, () -> new Grade(9001, 20));
		
		Grade testGrade = new Grade(50, 100);
		double testPercentage = 33d / 58d;
		
		TestException(expectedException1, () -> testGrade.setGrade(20, 0));
		TestException(expectedException2, () -> testGrade.setGrade(-1, 20));
		TestException(expectedException3, () -> testGrade.setGrade(9001, 20));
		
		assertEquals(50, testGrade.getEarnedPoints());
		assertEquals(100, testGrade.getAvailablePoints());
		
		testGrade.setGrade(33, 58);
		
		assertEquals(33, testGrade.getEarnedPoints());
		assertEquals(58, testGrade.getAvailablePoints());
		assertEquals(testPercentage, testGrade.getPercentage());
	}
	
	@Test
	void testToString() {
		String expectedString1 = "85:100 -- 85.00%";
		Grade testGrade1 = new Grade(85, 100);
		assertEquals(expectedString1, testGrade1.toString());
		
		String expectedString2 = "99:100 -- 99.00%";
		Grade testGrade2 = new Grade(99, 100);
		assertEquals(expectedString2, testGrade2.toString());
		
		String expectedString3 = "110:115 -- 95.65%";
		Grade testGrade3 = new Grade(110, 115);
		assertEquals(expectedString3, testGrade3.toString());
		
		String expectedString4 = "125:150 -- 83.33%";
		Grade testGrade4 = new Grade(125, 150);
		assertEquals(expectedString4, testGrade4.toString());
	}
	
	private static final void TestException(String expected, Runnable task) {
		try {
			task.run();
		}
		catch(IllegalArgumentException e) {
			assertEquals(expected, e.getMessage());
		}
	}

}
