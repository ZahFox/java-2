package ui;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import java.awt.Font;

import javax.swing.DefaultListModel;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JButton;
import javax.swing.JComboBox;

import java.awt.BorderLayout;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.JTextPane;

public class GPACalculator extends XModalFrame {
	
	protected static final double[] GRADE_NUMBERS = {4.00D, 3.50D, 3.00D, 2.50D, 2.00D, 1.00D, 0.00D};
	protected static final String[] GRADE_LETTERS = {"A", "A/B", "B", "B/C", "C", "D", "F"};
	
	protected JTextField txtCourseId;
	protected JTextField txtCredits;
	protected JTextField txtCumulativeGPA;
	
	protected JComboBox cmbGradeEarned;
	
	protected JButton btnAddGrade;
	protected JButton btnClear;
	
	protected JScrollPane scrGradesEntered;
	protected DefaultListModel<String> gradesListModel;
	protected JList lstGradesEntered;
	protected JList lstGradeScale;
	
	public GPACalculator(JFrame parent) {
		super(parent);
	}
	
	/**
	 * @wbp.parser.entryPoint
	 */
	@Override
	public void initialize() {
		this.frame = new JFrame();
		this.frame.setBackground(Color.WHITE);
		this.frame.setTitle("GPA Calculator");
		this.frame.setBounds(100, 100, 695, 665);
		this.frame.getContentPane().setLayout(null);
		
		JLabel lblCourseId = new JLabel("Course Id:");
		lblCourseId.setFont(new Font("Monaco", Font.BOLD, 16));
		lblCourseId.setBounds(50, 50, 150, 25);
		frame.getContentPane().add(lblCourseId);
		
		txtCourseId = new JTextField();
		txtCourseId.setFont(new Font("Monaco", Font.BOLD, 16));
		txtCourseId.setForeground(Color.BLUE);
		txtCourseId.setBounds(225, 50, 150, 25);
		frame.getContentPane().add(txtCourseId);
		txtCourseId.setColumns(30);
		
		JLabel lblGradeScale = new JLabel("Grade Scale:");
		lblGradeScale.setFont(new Font("Monaco", Font.BOLD, 16));
		lblGradeScale.setBounds(425, 50, 150, 25);
		frame.getContentPane().add(lblGradeScale);
		
		DefaultListModel gradesScaleModel = new DefaultListModel<String>();
		for (int i = 0; i < GRADE_NUMBERS.length; i++) {
			gradesScaleModel.addElement(String.format("%.2f %s", GRADE_NUMBERS[i], GRADE_LETTERS[i]));
		}
		lstGradeScale = new JList<String>(gradesScaleModel);
		lstGradeScale.setFont(new Font("Monaco", Font.BOLD, 14));
		lstGradeScale.setBounds(425, 100, 200, 200);
		lstGradeScale.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		lstGradeScale.setSelectionModel(new NoSelectionModel());
		frame.getContentPane().add(lstGradeScale);
		
		JLabel lblCredits = new JLabel("Credits:");
		lblCredits.setFont(new Font("Monaco", Font.BOLD, 16));
		lblCredits.setBounds(50, 100, 150, 25);
		frame.getContentPane().add(lblCredits);
		
		txtCredits = new JTextField();
		txtCredits.setFont(new Font("Monaco", Font.BOLD, 16));
		txtCredits.setForeground(Color.BLUE);
		txtCredits.setBounds(225, 100, 150, 25);
		frame.getContentPane().add(txtCredits);
		txtCredits.setColumns(10);
		
		JLabel lblGradesEarned = new JLabel("Grade Earned:");
		lblGradesEarned.setFont(new Font("Monaco", Font.BOLD, 16));
		lblGradesEarned.setBounds(50, 150, 150, 25);
		frame.getContentPane().add(lblGradesEarned);
		
		
		cmbGradeEarned = new JComboBox(GRADE_LETTERS);
		cmbGradeEarned.setFont(new Font("Monaco", Font.BOLD, 16));
		cmbGradeEarned.setForeground(Color.BLUE);
		cmbGradeEarned.setBounds(225, 150, 150, 25);
		frame.getContentPane().add(cmbGradeEarned);
		
		
		btnAddGrade = new JButton("Add Grade");
		btnAddGrade.setFont(new Font("Monaco", Font.BOLD, 12));
		btnAddGrade.setForeground(Color.WHITE);
		btnAddGrade.setBackground(Color.DARK_GRAY);
		btnAddGrade.setBounds(50, 215, 150, 25);
		frame.getContentPane().add(btnAddGrade);
		
		btnClear = new JButton("Clear");
		btnClear.setFont(new Font("Monaco", Font.BOLD, 12));
		btnClear.setForeground(Color.WHITE);
		btnClear.setBackground(Color.DARK_GRAY);
		btnClear.setBounds(225, 215, 150, 25);
		frame.getContentPane().add(btnClear);
		
		JLabel lblCumulativeGPA = new JLabel("Cumulative GPA:");
		lblCumulativeGPA.setFont(new Font("Monaco", Font.BOLD, 16));
		lblCumulativeGPA.setBounds(50, 280, 150, 25);
		frame.getContentPane().add(lblCumulativeGPA);
		
		txtCumulativeGPA = new JTextField();
		txtCumulativeGPA.setFont(new Font("Monaco", Font.BOLD, 16));
		txtCumulativeGPA.setForeground(Color.BLUE);
		txtCumulativeGPA.setBounds(225, 280, 150, 25);
		txtCumulativeGPA.setEditable(false);
		frame.getContentPane().add(txtCumulativeGPA);
		txtCumulativeGPA.setColumns(30);
		
		JLabel lblGradesEntered = new JLabel("Grades Entered:");
		lblGradesEntered.setFont(new Font("Monaco", Font.BOLD, 16));
		lblGradesEntered.setBounds(50, 330, 150, 25);
		frame.getContentPane().add(lblGradesEntered);
		
		scrGradesEntered = new JScrollPane();
		scrGradesEntered.setBounds(50, 380, 575, 200);
		scrGradesEntered.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		frame.getContentPane().add(scrGradesEntered);
		
		gradesListModel = new DefaultListModel<String>();
		lstGradesEntered = new JList<String>(gradesListModel);
		lstGradesEntered.setFont(new Font("Monaco", Font.BOLD, 14));
		scrGradesEntered.setViewportView(lstGradesEntered);
	}

}
