package ui;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

abstract public class ModalFrame {

	protected JFrame frame;
	protected JFrame parent;
	
	public ModalFrame(JFrame parent) {
		this.parent = parent;
		
		initialize();
		
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		ImageIcon img = new ImageIcon("launch-icon.png");
		frame.setIconImage(img.getImage());
		
		frame.addWindowListener(new WindowAdapter() {
		    @Override
		    public void windowClosing(WindowEvent windowEvent) {
		    	disableFrame();
		    }
		});
		
		enableFrame();
	}
	
	protected abstract void initialize();
	
	public void enableFrame() {
		frame.setLocation(parent.getLocation());
		parent.setVisible(false);
		frame.setVisible(true);
	}
	
	public void disableFrame() {
		parent.setLocation(frame.getLocation());
		frame.setVisible(false);
		parent.setVisible(true);
	}
	
}
