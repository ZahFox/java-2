package ui;

import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

import models.ClassGrade;

public class GPACalculatorX extends GPACalculator {

	private static final String COURSE_ID_TAKEN = "This Course Id is already taken please choose a different one.";
	private static final String UNEXPECTED_ERROR_NEW_GRADE = "There was an unexpected error while trying to add a new grade!";
	
	private HashMap<String, ClassGrade> grades;
	
	public GPACalculatorX(JFrame parent) {
		super(parent);
		
		grades = new HashMap<String, ClassGrade>();
		
		btnAddGrade.addActionListener( (args) -> {
			new AddGradeTask(this).run();
		});
		
		btnClear.addActionListener( (args) -> {
			new ClearTask(this).run();
		});
	}
	
	public void addNewGrade(ClassGrade newClassGrade) {
		this.grades.put(newClassGrade.getClassId(), newClassGrade);
		this.gradesListModel.addElement(newClassGrade.toString());
	}
	
	public void calculateCumulativeGPA() {
		double cumulativeGPA =
				this.grades.values()
				.stream().mapToDouble(ClassGrade::getGradePoints)
				.sum() /
				this.grades.values()
				.stream().mapToDouble(grade -> grade.getCredits() * 4)
				.sum() * 4.00D;   
				
		this.txtCumulativeGPA.setText(String.format("%.2f", cumulativeGPA));
	}
	
	public ClassGrade validateInput() {
		try {
			String courseId = this.txtCourseId.getText().trim();	
			if (courseIdIsNotTaken(courseId)) {
				this.txtCourseId.setText(courseId);
				int credits = parseTextFieldInput(this.txtCredits, "Grade's", "credits");
				double gradeEarned = gradeLetterToValue((String)this.cmbGradeEarned.getSelectedItem());
				return new ClassGrade(courseId, credits, gradeEarned);
			}
			else return null;
		}
		catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(this.frame, e.getMessage());
			return null;
		}
		catch (IllegalArgumentException e) {
			handleIllegalArgumentException(e.getMessage());
			return null;
		}
		catch (Exception e) {
			JOptionPane.showMessageDialog(this.frame, UNEXPECTED_ERROR_NEW_GRADE);
			return null;
		}
	}
	
	private void handleIllegalArgumentException(String exception) {
		JOptionPane.showMessageDialog(this.frame, exception);
		
		switch (exception) {
			case COURSE_ID_TAKEN:
				clearTextField(this.txtCourseId);
				break;	
			case ClassGrade.INVALID_CREDITS:
				clearTextField(this.txtCredits);
				break;
			case ClassGrade.INVALID_GRADE:
				this.cmbGradeEarned.setSelectedIndex(0);
				break;
		}
	}
	
	private boolean courseIdIsNotTaken(String courseId) {
		for (String id : this.grades.keySet()) {
			if (courseId.equals(id)) throw new IllegalArgumentException(COURSE_ID_TAKEN);
		}
		return true;
	}
	
	private double gradeLetterToValue(String gradeLetter) {
		for (int i = 0; i < GRADE_LETTERS.length; i++) {
			if (GRADE_LETTERS[i].equals(gradeLetter))
				return GRADE_NUMBERS[i];
		}
		return -1;
	}
	
	public void clearGrades() {
		this.grades.clear();
		this.gradesListModel.clear();
		this.cmbGradeEarned.setSelectedIndex(0);
		clearTextField(this.txtCumulativeGPA);
		clearTextField(this.txtCredits);
		clearTextField(this.txtCourseId);
	}
	
	private class AddGradeTask extends SwingWorker<Void, Void> {
		private GPACalculatorX context;
		
		public AddGradeTask(GPACalculatorX context) {
			super();
			this.context = context;
		}
		
		@Override
		protected Void doInBackground() throws Exception {
			Thread thread = new Thread( () -> {
				toggleButton(context.btnAddGrade);
				
				ClassGrade newClassGrade = context.validateInput();
				
				if (newClassGrade != null) {
					clearTextField(context.txtCredits);
					clearTextField(context.txtCourseId);
					context.cmbGradeEarned.setSelectedIndex(0);
					context.addNewGrade(newClassGrade);
					context.calculateCumulativeGPA();
				}
				
				toggleButton(context.btnAddGrade);
			});
			
			thread.start();
			return null;
		}
	}
	
	private class ClearTask extends SwingWorker<Void, Void> {
		private GPACalculatorX context;
		
		public ClearTask(GPACalculatorX context) {
			super();
			this.context = context;
		}
		
		@Override
		protected Void doInBackground() throws Exception {
			Thread thread = new Thread( () -> {
				toggleButton(context.btnClear);
				context.clearGrades();
				toggleButton(context.btnClear);
			});
			
			thread.start();
			return null;
		}
	}

}
