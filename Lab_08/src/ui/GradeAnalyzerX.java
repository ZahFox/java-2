package ui;

import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollBar;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

import models.Grade;

public class GradeAnalyzerX extends GradeAnalyzer {
	
	private static final String UNEXPECTED_ERROR_NEW_GRADE = "There was an unexpected error while trying to add a new grade!";
	
	private ArrayList<Grade> grades;

	public GradeAnalyzerX(JFrame parent) {
		super(parent);
		
		grades = new ArrayList<Grade>();
		
		btnAddGrade.addActionListener( (args) -> {
			new AddGradeTask(this).run();
		});
		
		btnClearGrades.addActionListener( (args) -> {
			new ClearGradeTask(this).run();
		});
	}
	
	public void addNewGrade(Grade newGrade) {
		this.grades.add(newGrade);
		this.gradesListModel.addElement(newGrade.toString());
	}
	
	public void clearGrades() {
		clearTextField(this.txtAvailablePoints);
		clearTextField(this.txtEarnedPoints);
		this.grades.clear();
		this.gradesListModel.clear();
		this.clearSummary();
	}
	
	private void generateSummary() {
		double averageGrade = this.grades
			.stream().mapToDouble(Grade::getPercentage)
			.average().orElse(Double.NaN) * 100;
		
		double maxGrade = this.grades
			.stream().mapToDouble(Grade::getPercentage)
			.max().orElse(Double.NaN) * 100;
		
		double minGrade = this.grades
			.stream().mapToDouble(Grade::getPercentage)
			.min().orElse(Double.NaN) * 100;
		
		double totalGrade = (this.grades
			.stream().mapToDouble(Grade::getEarnedPoints)
			.sum() / this.grades
			.stream().mapToDouble(Grade::getAvailablePoints)
			.sum()) * 100;
		
		this.txpSummary.setText(
			String.format(
			"Average Grade: %.2f%%%nMax Grade: %.2f%%%nMin Grade: %.2f%%%nTotal Grade: %.2f%%",
			averageGrade, maxGrade, minGrade, totalGrade)
		);
	}
	
	private void clearSummary() {
		this.txpSummary.setText("");
	}
	
	private void handleIllegalArgumentException(String exception) {
		JOptionPane.showMessageDialog(this.frame, exception);
		
		switch (exception) {
			case Grade.EARNED_POINTS_TOO_SMALL:
				clearTextField(this.txtEarnedPoints);
				break;
			case Grade.EARNED_POINTS_TOO_BIG:
				clearTextField(this.txtEarnedPoints);
				break;
			case Grade.AVAILABLE_POINTS_TOO_SMALL:
				clearTextField(this.txtAvailablePoints);
				break;
		}
	}
	
	public Grade validateInput() {
		try {
			int earnedPoints = parseTextFieldInput(this.txtEarnedPoints, "Grade's", "earned points");
			int availablePoints = parseTextFieldInput(this.txtAvailablePoints, "Grade's", "available points");
			return new Grade(earnedPoints, availablePoints);
		}
		catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(this.frame, e.getMessage());
			return null;
		}
		catch (IllegalArgumentException e) {
			handleIllegalArgumentException(e.getMessage());
			return null;
		}
		catch (Exception e) {
			JOptionPane.showMessageDialog(this.frame, UNEXPECTED_ERROR_NEW_GRADE);
			return null;
		}
	}
	
	private class AddGradeTask extends SwingWorker<Void, Void> {
		
		private GradeAnalyzerX context;
		
		public AddGradeTask(GradeAnalyzerX context) {
			super();
			this.context = context;
		}
		
		@Override
		protected Void doInBackground() throws Exception {
			Thread thread = new Thread( () -> {
				toggleButton(context.btnAddGrade);
				
				Grade newGrade = context.validateInput();
				
				if (newGrade != null) {
					clearTextField(context.txtAvailablePoints);
					clearTextField(context.txtEarnedPoints);
					context.addNewGrade(newGrade);
					JScrollBar scrollBar = context.scrGradeList.getVerticalScrollBar();
					scrollBar.setValue(scrollBar.getMaximum());
					context.lstGradeList.setSelectedIndex(context.gradesListModel.size() - 1);
					context.generateSummary();
				}
				
				toggleButton(context.btnAddGrade);
			});
			
			thread.start();
			return null;
		}
	}
	
	private class ClearGradeTask extends SwingWorker<Void, Void> {
		
		private GradeAnalyzerX context;
		
		public ClearGradeTask(GradeAnalyzerX context) {
			super();
			this.context = context;
		}
		
		@Override
		protected Void doInBackground() throws Exception {
			Thread thread = new Thread( () -> {
				toggleButton(context.btnClearGrades);
				context.clearGrades();
				toggleButton(context.btnClearGrades);
			});
			
			thread.start();
			return null;
		}
	}
	
}
