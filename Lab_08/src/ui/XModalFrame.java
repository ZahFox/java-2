package ui;

import javax.swing.DefaultListSelectionModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public abstract class XModalFrame extends ModalFrame {

	public XModalFrame(JFrame parent) {
		super(parent);
	}

	public abstract void initialize();
	
	protected void clearTextField(JTextField field) {
		field.setText("");
		field.requestFocus();
	}
	
	protected int parseTextFieldInput(JTextField field, String modelName, String fieldName) throws NumberFormatException {
		try {
			return Integer.parseInt(field.getText().trim());
		}
		catch (NumberFormatException e) {
			clearTextField(field);
			throw new NumberFormatException(String.format("A %s %s must be an integer!", modelName, fieldName));
		}
	}
	
	protected void toggleButton(JButton button) {
		button.setEnabled(!button.isEnabled());
	}
	
	protected class NoSelectionModel extends DefaultListSelectionModel {
		
	   @Override
	   public void setAnchorSelectionIndex(final int anchorIndex) {}

	   @Override
	   public void setLeadAnchorNotificationEnabled(final boolean flag) {}

	   @Override
	   public void setLeadSelectionIndex(final int leadIndex) {}

	   @Override
	   public void setSelectionInterval(final int index0, final int index1) { }
	}

}
