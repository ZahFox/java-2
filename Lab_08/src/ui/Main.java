package ui;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.EventQueue;

public class Main {

	private static final String GRADE_ANALYZER_LAUNCH_ERROR = "There was an unexpected error while trying to launch the Grade Analyzer form!";
	private static final String GPA_CALCULATOR_LAUNCH_ERROR = "There was an unexpected error while trying to launch the GPA Calculator form!";
	
	private JFrame frmMain;
	private ModalFrame frmGradeAnalyzer = null;
	private ModalFrame frmGPACalculator = null;
	private JButton btnGradeAnalyzer;
	private JButton btnGPACalculator;

	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
		bindActionListeners();
		ImageIcon img = new ImageIcon("launch-icon.png");
		frmMain.setIconImage(img.getImage());
		frmMain.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMain = new JFrame();
		frmMain.getContentPane().setBackground(Color.WHITE);
		frmMain.setFont(new Font("Consolas", Font.BOLD, 12));
		frmMain.setTitle("Grade Analyzer");
		frmMain.setBounds(100, 100, 450, 300);
		frmMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMain.getContentPane().setLayout(null);
		
		btnGradeAnalyzer = new JButton("Grade Analyzer");
		btnGradeAnalyzer.setFont(new Font("Monaco", Font.BOLD, 12));
		btnGradeAnalyzer.setForeground(Color.WHITE);
		btnGradeAnalyzer.setBackground(Color.DARK_GRAY);
		btnGradeAnalyzer.setBounds(140, 75, 150, 25);
		frmMain.getContentPane().add(btnGradeAnalyzer);
		
		btnGPACalculator = new JButton("GPA Calculator");
		btnGPACalculator.setFont(new Font("Monaco", Font.BOLD, 12));
		btnGPACalculator.setForeground(Color.WHITE);
		btnGPACalculator.setBackground(Color.DARK_GRAY);
		btnGPACalculator.setBounds(140, 130, 150, 25);
		frmMain.getContentPane().add(btnGPACalculator);
	}
	
	private void bindActionListeners() {
		btnGradeAnalyzer.addActionListener(args -> lanchGradeAnalyzerForm());
		btnGPACalculator.addActionListener(args -> launchGPACalculatorForm());
	}
	
	private void lanchGradeAnalyzerForm() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					if (frmGradeAnalyzer == null) {
						frmGradeAnalyzer = new GradeAnalyzerX(frmMain);
					}
					else {
						frmGradeAnalyzer.enableFrame();
					}	
				} catch (Exception e) {
					JOptionPane.showMessageDialog(frmMain, GRADE_ANALYZER_LAUNCH_ERROR);
					e.printStackTrace();
				}
			}
		});
	}
	
	private void launchGPACalculatorForm() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					if (frmGPACalculator == null) {
						frmGPACalculator = new GPACalculatorX(frmMain);
					}
					else {
						frmGPACalculator.enableFrame();
					}
				} catch (Exception e) {
					JOptionPane.showMessageDialog(frmMain, GPA_CALCULATOR_LAUNCH_ERROR);
					e.printStackTrace();
				}
			}
		});
	}
}
