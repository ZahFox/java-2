package ui;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import java.awt.Font;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.BorderLayout;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.JTextPane;

public class GradeAnalyzer extends XModalFrame {
	protected JTextField txtEarnedPoints;
	protected JTextField txtAvailablePoints;
	
	protected JButton btnAddGrade;
	protected JButton btnClearGrades;
	
	protected JScrollPane scrGradeList;
	protected JList<String> lstGradeList;
	protected DefaultListModel<String> gradesListModel;
	protected JTextPane txpSummary;

	public GradeAnalyzer(JFrame parent) {
		super(parent);
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	@Override
	public void initialize() {
		this.frame = new JFrame();
		this.frame.setBackground(Color.WHITE);
		this.frame.setTitle("Grade Analyzer");
		this.frame.setBounds(100, 100, 725, 500);
		this.frame.getContentPane().setLayout(null);
		
		JLabel lblEnterGrade = new JLabel("Enter Grade:");
		lblEnterGrade.setFont(new Font("Monaco", Font.BOLD, 16));
		lblEnterGrade.setBounds(50, 50, 150, 25);
		frame.getContentPane().add(lblEnterGrade);
		
		txtEarnedPoints = new JTextField();
		txtEarnedPoints.setFont(new Font("Monaco", Font.BOLD, 16));
		txtEarnedPoints.setForeground(Color.BLUE);
		txtEarnedPoints.setBounds(200, 50, 100, 25);
		frame.getContentPane().add(txtEarnedPoints);
		txtEarnedPoints.setColumns(10);
		
		JLabel lblOutOf = new JLabel("out of");
		lblOutOf.setFont(new Font("Monaco", Font.BOLD, 16));
		lblOutOf.setBounds(325, 50, 150, 25);
		frame.getContentPane().add(lblOutOf);
		
		txtAvailablePoints = new JTextField();
		txtAvailablePoints.setFont(new Font("Monaco", Font.BOLD, 16));
		txtAvailablePoints.setForeground(Color.BLUE);
		txtAvailablePoints.setBounds(400, 50, 100, 25);
		frame.getContentPane().add(txtAvailablePoints);
		txtAvailablePoints.setColumns(10);
		
		JLabel lblPoints = new JLabel("points");
		lblPoints.setFont(new Font("Monaco", Font.BOLD, 16));
		lblPoints.setBounds(515, 50, 150, 25);
		frame.getContentPane().add(lblPoints);
		
		btnAddGrade = new JButton("Add Grade");
		btnAddGrade.setFont(new Font("Monaco", Font.BOLD, 16));
		btnAddGrade.setForeground(Color.WHITE);
		btnAddGrade.setBackground(Color.DARK_GRAY);
		btnAddGrade.setBounds(50, 100, 250, 25);
		frame.getContentPane().add(btnAddGrade);
		
		btnClearGrades = new JButton("Clear Grades");
		btnClearGrades.setFont(new Font("Monaco", Font.BOLD, 16));
		btnClearGrades.setForeground(Color.WHITE);
		btnClearGrades.setBackground(Color.DARK_GRAY);
		btnClearGrades.setBounds(400, 100, 250, 25);
		frame.getContentPane().add(btnClearGrades);
		
		JLabel lblGradesEntered = new JLabel("Grades Entered:");
		lblGradesEntered.setFont(new Font("Monaco", Font.BOLD, 16));
		lblGradesEntered.setBounds(50, 150, 200, 25);
		frame.getContentPane().add(lblGradesEntered);
		
		JLabel lblGradeSummary = new JLabel("Grade Summary:");
		lblGradeSummary.setFont(new Font("Monaco", Font.BOLD, 16));
		lblGradeSummary.setBounds(400, 150, 200, 25);
		frame.getContentPane().add(lblGradeSummary);
		
		scrGradeList = new JScrollPane();
		scrGradeList.setBounds(50, 200, 250, 200);
		frame.getContentPane().add(scrGradeList);
		
		gradesListModel = new DefaultListModel<String>();
		lstGradeList = new JList<String>(gradesListModel);
		lstGradeList.setFont(new Font("Monaco", Font.BOLD, 14));
		scrGradeList.setViewportView(lstGradeList);
		
		JPanel pnlSummary = new JPanel();
		pnlSummary.setBackground(Color.GRAY);
		pnlSummary.setForeground(Color.WHITE);
		pnlSummary.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		pnlSummary.setBounds(400, 200, 250, 200);
		frame.getContentPane().add(pnlSummary);
		pnlSummary.setLayout(null);
		
		txpSummary = new JTextPane();
		txpSummary.setFont(new Font("Monaco", Font.BOLD, 14));
		txpSummary.setBounds(0, 0, 250, 200);
		txpSummary.setEditable(false);
		pnlSummary.add(txpSummary);
	}
}
