package models;

import java.util.stream.DoubleStream;

public class ClassGrade {
	
	private static final double[] ACCEPTABLE_GRADES = {0.00D, 1.00D, 2.00D, 2.50D, 3.00D, 3.50D, 4.00D};
	
	public static final String INVALID_COURSE_ID = "A Grade's course Id must be included!";
	public static final String INVALID_CREDITS = "A Grade's credits must be a positive integer!";
	public static final String INVALID_GRADE = "A Grade's grade earned must be one of the acceptable grades!";
	
	private String classId;
	private int credits;
	private double grade;
	
	public ClassGrade(String classId, int credits, double grade) {
		this.setClassId(classId);
		this.setCredits(credits);
		this.setGrade(grade);
	}
	
	public String getClassId() {
		return classId;
	}

	public void setClassId(String classId) throws IllegalArgumentException {
		if (classId.equals("") || classId.equals(null)) throw new IllegalArgumentException(INVALID_COURSE_ID);
		this.classId = classId;
	}

	public int getCredits() {
		return credits;
	}

	public void setCredits(int credits) throws IllegalArgumentException {
		if (credits < 1) throw new IllegalArgumentException(INVALID_CREDITS);
		this.credits = credits;
	}

	public double getGrade() {
		return grade;
	}

	public void setGrade(double grade) throws IllegalArgumentException {
		if (!DoubleStream.of(ACCEPTABLE_GRADES).anyMatch(value -> value == grade)) throw new IllegalArgumentException(INVALID_GRADE);
		this.grade = grade;
	}
	
	public double getGradePoints() {
		return this.grade * this.credits;
	}
	
	@Override
	public String toString() {
		return String.format("Class: %s Credits: %d Grade: %.2f Grade Points: %.2f",
			this.getClassId(), this.getCredits(), this.getGrade(), this.getGradePoints());
	}

}
