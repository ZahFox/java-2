package models;

public class Grade {
	
	public static final String AVAILABLE_POINTS_TOO_SMALL = "A Grade's avaiable points must be greater than zero!";
	public static final String EARNED_POINTS_TOO_SMALL = "A Grade's earned points must be greater than or equal to zero!";
	public static final String EARNED_POINTS_TOO_BIG = "A Grade's earned points must be less than or equal to the available points!";
	
	private int earnedPoints;
	private int availablePoints;
	
	public Grade(int earnedPoints, int availablePoints) {
		this.setGrade(earnedPoints, availablePoints);
	}
	
	public void setGrade(int earnedPoints, int availablePoints) throws IllegalArgumentException {
		if (availablePoints <= 0) throw new IllegalArgumentException(AVAILABLE_POINTS_TOO_SMALL);
		if (earnedPoints < 0) throw new IllegalArgumentException(EARNED_POINTS_TOO_SMALL);
		if (earnedPoints > availablePoints) throw new IllegalArgumentException(EARNED_POINTS_TOO_BIG);
		this.earnedPoints = earnedPoints;
		this.availablePoints = availablePoints;
	}
	
	public double getPercentage() {
		return this.earnedPoints / (double)this.availablePoints;
	}
	
	public int getEarnedPoints() {
		return this.earnedPoints;
	}
	
	public int getAvailablePoints() {
		return this.availablePoints;
	}
	
	@Override
	public String toString() {
		return String.format("%d:%d -- %.2f%%",
			this.getEarnedPoints(), this.getAvailablePoints(), this.getPercentage() * 100);
	}

}
