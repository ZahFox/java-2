package testing;

import static org.junit.jupiter.api.Assertions.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.Test;
import application.*;


class Person_Test {
	
	void whatever() {
		assertEquals(1, 1);
	}
	
	@Test
	void testBasics() {
		
		Student s1       = new Student();
		Student s2       = new Student("Carl", 33);
		Student s3       = new Student("Fred", 42);
		Student s4       = new Student("Fred", 42);
		
		
		Undergraduate u1 = new Undergraduate();
		Undergraduate u2 = new Undergraduate("Carl", 33, 1);
		Undergraduate u3 = new Undergraduate("Fred", 42, 4);
		Undergraduate u4 = new Undergraduate("Fred", 42, 4);
		
		Faculty f1       = new Faculty();
		Faculty f2       = new Faculty("Carl", 33, "TEST", "BOSS");
		Faculty f3       = new Faculty("Fred", 42, "TEST", "BOSS");
		Faculty f4       = new Faculty("Fred", 42, "TEST", "BOSS");
		
		Staff st1        = new Staff();
		Staff st2        = new Staff("Carl", 33, "TEST", 4);
		Staff st3        = new Staff("Fred", 42, "TEST", 4);
		Staff st4        = new Staff("Fred", 42, "TEST", 4);
		
		Student[] students         = new Student[]       {s1, s2, s3, s4};
		Undergraduate[] undergrads = new Undergraduate[] {u1, u2, u3, u4};
		Faculty[] faculty          = new Faculty[]       {f1, f2, f3, f4};
		Staff[] staff              = new Staff[]         {st1, st2, st3, st4};
		
		for (int i = 0; i < students.length; ++i) {
			assertEquals(students[i].getName(), undergrads[i].getName());		
			assertEquals(true, students[i].hasSameName(undergrads[i]));
		}
		
		assertEquals(true, s3.hasSameName(s4));
		assertEquals(true, u3.hasSameName(u4));
		assertEquals(true, f3.hasSameName(f4));
		assertEquals(true, st3.hasSameName(st4));

	}
	
	
	@Test
	void testEquals() {
		
		Student s1       = new Student("Fred", 42);
		Student s2       = new Student("Fred", 42);
		Undergraduate u1 = new Undergraduate("Fred", 42, 4);
		Undergraduate u2 = new Undergraduate("Fred", 42, 4);
		Faculty f1       = new Faculty("Sam", 33, "Test1", "Test2");
		Faculty f2       = new Faculty("Can", 33, "Test3", "Test4");
		Faculty f3       = new Faculty("Tan", 42, "Test5", "Test6");
		Staff st1        = new Staff("Sam", 33, "Test1", 1);
		Staff st2        = new Staff("Can", 33, "Test2", 2);
		Staff st3        = new Staff("Tan", 42, "Test3", 3);
		
		assertEquals(false, s1.equals(null));
		assertEquals(true,  s1.equals(s2));
		assertEquals(false, u1.equals(null));
		assertEquals(true,  u1.equals(u2));
		assertEquals(true,  u1.equals(s2));
		assertEquals(true,  s1.equals(u2));
		assertEquals(false, f1.equals(null));
		assertEquals(true,  f1.equals(f2));
		assertEquals(false, f2.equals(f3));
		assertEquals(false, f1.equals(f3));
		assertEquals(false, st1.equals(null));
		assertEquals(true,  st1.equals(st2));
		assertEquals(false, st2.equals(st3));
		assertEquals(false, st1.equals(st3));
		
	}
	
	
	@Test
	void testSetters() {
		
		Student s       = new Student();
		Undergraduate u = new Undergraduate();
		Faculty f       = new Faculty();
		Staff st        = new Staff();
		
		s.setName(null);
		assertEquals("", s.getName());
		s.setStudentNumber(1);
		assertEquals(1, s.getStudentNumber()); 
		s.setStudentNumber(90000);
		assertEquals(90000, s.getStudentNumber());
		RuntimeException e1 = Testing_Utils.expectException( IllegalArgumentException.class, () -> {
			s.setStudentNumber(-1);
		});
		assertEquals("Error: A student's number must be a positive number.", e1.getMessage());
		
		u.setName(null);
		assertEquals("", u.getName());
		u.setLevel(1);
		assertEquals(1, u.getLevel()); 
		u.setLevel(4);
		assertEquals(4, u.getLevel());
		RuntimeException e2 = Testing_Utils.expectException( IllegalArgumentException.class, () -> {
			u.setLevel(0);
		});
		assertEquals("Error: An undergraduate's number must be " +
                     "between one and four (inclusive).", e2.getMessage());
		RuntimeException e3 = Testing_Utils.expectException( IllegalArgumentException.class, () -> {
			u.setLevel(5);
		});
		assertEquals("Error: An undergraduate's number must be " +
                     "between one and four (inclusive).", e3.getMessage());
		
		f.setDepartment(null);
		assertEquals("", f.getDepartment());
		f.setName(null);
		assertEquals("", f.getName());
		f.setTitle(null);
		assertEquals("", f.getTitle());
		f.setEmployeeID(42);
		assertEquals(42, f.getEmployeeID()); 
		RuntimeException e4 = Testing_Utils.expectException( IllegalArgumentException.class, () -> {
			f.setEmployeeID(-1);
		});
		assertEquals("Error: An employee's number must be a positive number.", e4.getMessage());
		
		st.setDepartment(null);
		assertEquals("", st.getDepartment());
		st.setName(null);
		assertEquals("", st.getName());
		st.setPayGrade(1);
		assertEquals(1, st.getPayGrade());
		st.setPayGrade(20);
		assertEquals(20, st.getPayGrade());
		RuntimeException e5 = Testing_Utils.expectException( IllegalArgumentException.class, () -> {
			st.setPayGrade(-1);
		});
		assertEquals("Error: A staff's number must be " +
                     "between one and twenty (inclusive).", e5.getMessage());
		RuntimeException e6 = Testing_Utils.expectException( IllegalArgumentException.class, () -> {
			st.setPayGrade(21);
		});
		assertEquals("Error: A staff's number must be " +
                     "between one and twenty (inclusive).", e6.getMessage());
	}
	
	
	@Test
	void testWriteOutput() {
		
		final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));
		
		Student s       = new Student("Fred", 33);
		Undergraduate u = new Undergraduate("Fred", 33, 4);
		Faculty f       = new Faculty("Fred", 33, "Test", "Test");
		Staff st        = new Staff("Fred", 33, "Test", 10);
		
		// WARNING: These tests are only for Windows line-endings
		
		s.writeOutput();
		assertEquals("Name: Fred\r\nStudent Number: 33\r\n", outContent.toString());
		outContent.reset();
		
		u.writeOutput();
		assertEquals("Name: Fred\r\nStudent Number: 33\r\nStudent Level: 4\r\n", outContent.toString());
		outContent.reset();
		
		f.writeOutput();
		assertEquals("Name: Fred\r\nEmployee ID: 33\r\nDepartment: Test\r\nTitle: Test\r\n", outContent.toString());
		outContent.reset();
		
		st.writeOutput();
		assertEquals("Name: Fred\r\nEmployee ID: 33\r\nDepartment: Test\r\nPay Grade: 10\r\n", outContent.toString());
		outContent.reset();
		
		System.setOut(System.out);
		
	}
	
	
	@Test
	void testBubbleSort() {
		
		String testString =
		"Name: A\r\n" + 
		"Student Number: 1\r\nName: B\r\n" +
		"Student Number: 2\r\nName: C\r\n" +
		"Student Number: 3\r\nName: D\r\n" +
		"Student Number: 4\r\nName: E\r\n" +
		"Student Number: 5\r\nName: F\r\n" +
		"Student Number: 6\r\nName: G\r\n" +
		"Student Number: 7\r\nName: H\r\n" +
		"Student Number: 8\r\nName: I\r\n" +
		"Student Number: 9\r\nName: J\r\n" +
		"Student Number: 10\r\nName: K\r\n" +
		"Student Number: 11\r\nName: L\r\n" +
		"Student Number: 12\r\nName: M\r\n" +
		"Student Number: 13\r\nName: N\r\n" +
		"Student Number: 14\r\nName: O\r\n" +
		"Student Number: 15\r\nName: P\r\n" +
		"Student Number: 16\r\nName: Q\r\n" +
		"Student Number: 17\r\nName: R\r\n" +
		"Student Number: 18\r\nName: S\r\n" +
		"Student Number: 19\r\nName: T\r\n" +
		"Student Number: 20\r\nName: U\r\n" +
		"Student Number: 21\r\nName: V\r\n" +
		"Student Number: 22\r\nName: W\r\n" +
		"Student Number: 23\r\nName: X\r\n" +
		"Student Number: 24\r\nName: Y\r\n" +
		"Student Number: 25\r\nName: Z\r\n" +
		"Student Number: 26\r\n";
				
		Student[] students = new Student[26];
		students[0]  = new Student("A", 1);
		students[1]  = new Student("C", 3);
		students[2]  = new Student("E", 5);
		students[3]  = new Student("G", 7);
		students[4]  = new Student("I", 9);
		students[5]  = new Student("K", 11);
		students[6]  = new Student("M", 13);
		students[7]  = new Student("O", 15);
		students[8]  = new Student("Q", 17);
		students[9]  = new Student("S", 19);
		students[10] = new Student("U", 21);
		students[11] = new Student("W", 23);
		students[12] = new Student("Y", 25);
		students[13] = new Student("Z", 26);
		students[14] = new Student("X", 24);
		students[15] = new Student("V", 22);
		students[16] = new Student("T", 20);
		students[17] = new Student("R", 18);
		students[18] = new Student("P", 16);
		students[19] = new Student("N", 14);
		students[20] = new Student("L", 12);
		students[21] = new Student("J", 10);
		students[22] = new Student("H", 8);
		students[23] = new Student("F", 6);
		students[24] = new Student("D", 4);
		students[25] = new Student("B", 2);

		AppDriver.BubbleSort(students);
		
//		final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
//		System.setOut(new PrintStream(outContent));
//	
		for (Student s : students) {
			s.writeOutput();
		}
		//assertEquals(testString, outContent.toString());
		
//		System.setOut(System.out);
		
	}

}
