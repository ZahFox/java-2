package testing;

public final class Testing_Utils {

	public final static <T extends Throwable> T expectException( Class<T> exceptionClass, Runnable runnable ) {
	    try {
	        runnable.run();
	    }
	    catch( Exception throwable ) {
	        if( throwable instanceof Exception && throwable.getCause() != null )
	            throwable = (Exception) throwable.getCause(); //allows "assert x != null : new IllegalArgumentException();"
	        assert exceptionClass.isInstance( throwable ) : throwable; //exception of the wrong kind was thrown.
	        assert throwable.getClass() == exceptionClass : throwable; //exception thrown was a subclass, but not the exact class, expected.
	        @SuppressWarnings( "unchecked" )
	        T result = (T)throwable;
	        return result;
	    }
	    assert false; //expected exception was not thrown.
	    return null; //to keep the compiler happy.
	}
	
}
