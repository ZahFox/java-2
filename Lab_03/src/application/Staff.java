package application;

public class Staff extends Employee {

	private int payGrade;
	
	public Staff() {
		super();
		payGrade = 1;
	}
	
	public Staff(String initialName, int initialID, String initialDepartment, int initialPayGrade) {
		super(initialName, initialID, initialDepartment);
		setPayGrade(initialPayGrade);
	}
	
	public int getPayGrade() {
		return payGrade;
	}
	
	public void setPayGrade(int newPayGrade) throws IllegalArgumentException {
		if ( (1 <= newPayGrade) && (newPayGrade <= 20) ) {
			payGrade = newPayGrade;
		}
		else {
			throw new IllegalArgumentException("Error: A staff's number must be " +
                    "between one and twenty (inclusive).");
		}
	}
	
	public void writeOutput() {
		super.writeOutput();
		System.out.println("Pay Grade: " + payGrade);
	}
	
	public boolean equals(Staff otherStaff) {
		if (otherStaff != null)
			return this.getEmployeeID() == otherStaff.getEmployeeID();
		
		return false;
	}
	
}
