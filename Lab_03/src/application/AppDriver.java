package application;

import java.util.Arrays;

public class AppDriver {

	public static void main(String[] args) {
		Student[] students = new Student[5];
		
		students[0] = new Student("Muddle, Maninteh", 42);
		students[1] = new Student("Ket, Nick", 33);
		students[2] = new Student("Schiffings, Paquette", 13);
		students[3] = new Student("Pearl, Marco", 66);
		students[4] = new Student("Donuts, Ester", 7);
		
		//Arrays.sort(students);
		BubbleSort(students);
		
		for (Student s : students) {
			s.writeOutput();
			System.out.println();
		}
	}
	
	
	// Super Spicy Generic Sauce
	public static final <T> Comparable<T>[] BubbleSort(Comparable<T>[] values) {
		for (int i = 0; i < values.length - 1; ++i) {
		
		    boolean swapOccurred = false;
		
			for (int j = 1; j < values.length - i; ++j) {
				if ( values[j - 1].compareTo( (T) values[j] ) > 0 ) {
					Comparable<T> tmp = values[j - 1];
					values[j - 1] = values[j];
					values[j] = tmp;
					swapOccurred = true;
				}	
			}
			
			if (!swapOccurred) {
		        break;
			}
		}
		
		return values;
	}

}
