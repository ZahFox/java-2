package application;

public class Faculty extends Employee {

	private String title;
	
	public Faculty() {
		super();
		title = "";
	}
	
	public Faculty(String initialName, int initialID, String initialDepartment, String initialTitle) {
		super(initialName, initialID, initialDepartment);
		setTitle(initialTitle);
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String newTitle) {
		title = newTitle != null ? newTitle : new String("");
	}
	
	public void writeOutput() {
		super.writeOutput();
		System.out.println("Title: " + title);
	}
	
	public boolean equals(Faculty otherFaculty) {
		if (otherFaculty != null)
			return this.getEmployeeID() == otherFaculty.getEmployeeID();
		
		return false;
	}
	
}
