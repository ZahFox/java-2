package application;

public class Undergraduate extends Student {
	
	private int level; // 1 for freshman, 2 for sophomore, 3 for junior, or 4 for senior
	
	public Undergraduate() {
		super();
		level = 1;
	}
	
	public Undergraduate(String initialName, int initialStudentNumber, int initialLevel) {
		super(initialName, initialStudentNumber);
		setLevel(initialLevel);
	}
	
	public void reset(String newName, int newStudentNumber, int newLevel) {
		reset(newName, newStudentNumber);
		setLevel(newLevel);
	}
	
	public int getLevel() {
		return level;
	}
	
	public void setLevel(int newLevel) throws IllegalArgumentException {
		if ( (1 <= newLevel) && (newLevel <= 4) )
			level = newLevel;
		else {
			throw new IllegalArgumentException("Error: An undergraduate's number must be " +
                    "between one and four (inclusive).");
		}
	}
	
	public void writeOutput() {
		super.writeOutput();
		System.out.println("Student Level: " + level);
	}
	
	public boolean equals(Undergraduate otherUndergraduate) {
		if (otherUndergraduate != null) {
			return equals( (Student)otherUndergraduate ) &&
					  (this.level == otherUndergraduate.getLevel());
		}
		
		return false;
	}

}
