package application;

public class Student extends Person implements Comparable<Student> {
	
	private int studentNumber;
	
	public Student() {
		super();
		studentNumber = 0;
	}
	
	public Student(String initialName, int initialStudentNumber) {
		super(initialName);
		setStudentNumber(initialStudentNumber);
	}
	
	public void reset(String newName, int newStudentNumber) {
		setName(newName);
		studentNumber = newStudentNumber;
	}
	
	public int getStudentNumber() {
		return studentNumber;
	}
	
	public void setStudentNumber(int newStudentNumber) throws IllegalArgumentException {
		if (newStudentNumber >= 0) {
			studentNumber = newStudentNumber;
		}
		else {
			throw new IllegalArgumentException("Error: A student's number must be a positive number.");
		}
	}
	
	public void writeOutput() {
		super.writeOutput();
		System.out.println("Student Number: " + studentNumber);
	}
	
	public boolean equals(Student otherStudent) {
		if (otherStudent != null) {
			return this.hasSameName(otherStudent) && 
					  (this.getStudentNumber() == otherStudent.getStudentNumber());
		}
		
		return false;
	}

	// /**
	@Override // This version is for comparing based on studentNumber
	public int compareTo(Student otherStudent) {
		if (otherStudent != null) {
			if (studentNumber > otherStudent.getStudentNumber()) return 1;
			else if (studentNumber == otherStudent.getStudentNumber()) return 0;
			else return -1;
		}
		
		return -1;

	}
	// */
	
	/**
	@Override // This version is for comparing based on name
	public int compareTo(Student otherStudent) {
		if (otherStudent != null)
			return getName().compareTo(otherStudent.getName());
		
		return -1;
	}
	*/
	
}
