package application;

abstract class Person {

	private String name;
	
	public Person() {
		this("No name yet");
	}
	
	public Person(String initialName) {
		name = initialName;
	}
	
	public void setName(String newName) {
		name = newName != null ? newName : new String("");
	}
	
	public String getName() {
		return name;
	}
	
	public void writeOutput() {
		System.out.println("Name: " + name);
	}
	
	public boolean hasSameName(Person otherPerson) {
		if (otherPerson != null)
			return this.getName().equalsIgnoreCase(otherPerson.getName());
		
		return false;
	}
	
}
