package application;

abstract class Employee extends Person {

	private int employeeID;
	private String department;
	
	public Employee() {
		super();
		employeeID = 0;
	}
	
	public Employee(String initialName, int initialID, String initialDepartment) {
		super(initialName);
		setEmployeeID(initialID);
		setDepartment(initialDepartment);
	}
	
	public int getEmployeeID() {
		return employeeID;
	}
	
	public void setEmployeeID(int newEmployeeID) throws IllegalArgumentException {
		if (newEmployeeID >= 0) {
			employeeID = newEmployeeID;
		}
		else {
			throw new IllegalArgumentException("Error: An employee's number must be a positive number.");
		}
	}
	
	public String getDepartment() {
		return department;
	}
	
	public void setDepartment(String newDepartment ) {
		department = newDepartment != null ? newDepartment : new String("");
	}
	
	public void writeOutput() {
		super.writeOutput();
		System.out.println("Employee ID: " + employeeID);
		System.out.println("Department: " + department);
	}
	
}
