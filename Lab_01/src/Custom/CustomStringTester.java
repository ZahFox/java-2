package Custom;
public class CustomStringTester {
	
	/*---------------------------------------------------------------------------
	 * QUESTIONS:
	 * 1. Is my implementation of toUpperCase, endsWith, compareTo ok?
	 * 2. For source control, can all labs be in one repo?
	 *-------------------------------------------------------------------------*/
	
	
	public static void main(String[] args) {

		// test values
		CustomString hello = new CustomString("hello".toCharArray());
		CustomString emptyTest = new CustomString();
		CustomString uppercaseHello = new CustomString("HELLO".toCharArray());
		char[] testChars = {'h', 'e', 'l', 'l', 'o'};
		
		/////////////~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		/// TESTS ///
		/////////////~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		
		// test getSize()
		if (emptyTest.getSize() != 0 && hello.getSize() != 5) {
			Error("getSize does not work as intended!");
		}
		
		// test length()
		if (emptyTest.length() != 0 && hello.length() != 5) {
			Error("length does not work as intended!");
		}
		
		// test getCapacity()
		if(emptyTest.getCapacity() != 10 && hello.getCapacity() != 15) {
			Error("getCapacity does not work as intended!");
		}
		
		// test append()
		CustomString tap1 = new CustomString("part".toCharArray());
		CustomString tap2 = new CustomString("onetwothreefourfivesixseveneight".toCharArray());
		tap1.append(tap2.toCharArray());
		CustomString tap3 = new CustomString("ti".toCharArray());
		CustomString tap4 = new CustomString("helo".toCharArray());
		tap3.append(tap4.toCharArray());
		CustomString tap5 = new CustomString("hello".toCharArray());
		CustomString tap6 = new CustomString("".toCharArray());
		tap5.append(tap6.toCharArray());
		if ( !( tap1.equals(new CustomString("partonetwothreefourfivesixseveneight".toCharArray())) &&
				tap3.equals(new CustomString("tihelo".toCharArray())) &&
				tap5.equals(new CustomString("hello".toCharArray()))
				) ) {
			Error("append does not work as intended!");
		}
		
		// test isEmpty()
		if (!emptyTest.isEmpty()) {
			Error("isEmpty does not work as intended!");
		}
		
		// test charAt()
		if (emptyTest.charAt(0) != 0 && hello.charAt(0) == 'h') {
			Error("charAt does not work as intended!");
		}
		
		// test equals()
		if ( !(emptyTest.equals(hello) == false && uppercaseHello.equals(hello) == false && 
			hello.equals(hello) == true && emptyTest.equals(emptyTest) == true) ) {
			Error("equals does not work as intended!");
		}
		
		// test equalsIgnoreCase()
		if ( !hello.equalsIgnoreCase(uppercaseHello) ) {
			Error("equalsIgnoreCase does not work as intended!");
		}
		
		// test compareTo()
		CustomString com1 = new CustomString("ABC".toCharArray());
		CustomString com2 = new CustomString("abc".toCharArray());
		CustomString com3 = new CustomString("XYZ".toCharArray());
		CustomString com4 = new CustomString("ABCD".toCharArray());
		
		if ( !( com1.compareTo(com1) == 0 && com1.compareTo(com3) == -1 && com1.compareTo(com2) == -1 &&
				com1.compareTo(com4) == -1 && com1.compareTo(null) == 1 && com4.compareTo(com1) == 1 &&
				com4.compareTo(com3) == -1 && com3.compareTo(com4) == 1 && com2.compareTo(com3) == 1) ) {
			Error("compareTo does not work as intended!");
		}
		
		// test startsWith()
		CustomString helloWorld = new CustomString("helloWorld".toCharArray());
		if ( !( helloWorld.startsWith(hello) == true && hello.startsWith(null) == false ) ) {
			Error("startsWith does not work as intended!");
		}
		
		// test endsWith()
		if ( !( new CustomString("hamburger".toCharArray()).endsWith(new CustomString("burger".toCharArray())) == true &&
				new CustomString("hamburger".toCharArray()).endsWith(new CustomString("hamburgers".toCharArray())) == false &&
				new CustomString("hamburger".toCharArray()).endsWith(new CustomString("shamburger".toCharArray())) == false &&
				new CustomString("hamburger".toCharArray()).endsWith(new CustomString("ham".toCharArray())) == false &&
				new CustomString("redshoeshoeshoe".toCharArray()).endsWith(new CustomString("shoe".toCharArray())) == true &&
				new CustomString("hamburger".toCharArray()).endsWith(null) == false ) ) {
			Error("endsWith does not work as intended!");
		}
		
		// test indexOf()
		CustomString ti1 = new CustomString("ell".toCharArray());
		CustomString ti2 = new CustomString("ellos".toCharArray());
		CustomString ti3 = new CustomString("lo".toCharArray());
		CustomString ti4 = new CustomString("hellow".toCharArray());
		
		if ( !( hello.indexOf(ti1) == 1 && hello.indexOf(ti2) == -1 && hello.indexOf(ti3) == 3 &&
				hello.indexOf(null) == -1 && hello.indexOf(ti4) == -1 &&
				new CustomString("hamburger".toCharArray()).indexOf(new CustomString("burg".toCharArray())) == 3 &&
				new CustomString("Mississippi".toCharArray()).indexOf(new CustomString("iss".toCharArray())) == 1 &&
				new CustomString("Mississippi".toCharArray()).indexOf(new CustomString("ipp".toCharArray())) == 7 ) ) {
			Error("indexOf does not work as intended!");
		}
		
		
		// test substring()
		CustomString tss1 = new CustomString("hamburger".toCharArray());
		CustomString tss2 = new CustomString("smiles".toCharArray());
		CustomString stss1 = tss1.substring(4, 8);
		CustomString stss2 = tss2.substring(1, 5);
		if ( !( stss1.equals(new CustomString("urge".toCharArray())) &&
				stss2.equals(new CustomString("mile".toCharArray())) ) ) {
			Error("substring does not work as intended!");
		}
		
		// test replace()
		CustomString trp1 = new CustomString("bug".toCharArray());
		CustomString trp2 = new CustomString("The dog chased the dog".toCharArray());
		CustomString trp3 = new CustomString("The dog chased the dog".toCharArray());
		CustomString trp4 = new CustomString("The dog chased the dog".toCharArray());
		if ( !( 
				trp1.replace(new CustomString("u".toCharArray()), new CustomString("i".toCharArray())).equals(new CustomString("big".toCharArray())) == true &&
				trp2.replace(new CustomString("dog".toCharArray()), new CustomString("cat".toCharArray())).equals(new CustomString("The cat chased the cat".toCharArray())) == true &&
				trp3.replace(new CustomString("dog".toCharArray()), new CustomString("ox".toCharArray())).equals(new CustomString("The ox chased the ox".toCharArray())) == true &&
				trp4.replace(new CustomString("dog".toCharArray()), new CustomString("elephant".toCharArray())).equals(new CustomString("The elephant chased the elephant".toCharArray())) == true
		   ) ) {
			Error("replace does not work as intended!");
		}
		
		// test resize()
		CustomString trz1 = new CustomString("1234567890".toCharArray());
		CustomString trz2 = new CustomString("1234567890".toCharArray());
		trz1.resize(-1); trz2.resize(9000);
		if ( !( trz1.getCapacity() == 20 && trz2.getCapacity() == 9000 ) ) {
			Error("resize does not work as intended!");
		}
		
		// test contains()
		if ( !( new CustomString("hamburger".toCharArray()).contains(new CustomString("burger".toCharArray())) == true &&
				new CustomString("hamburger".toCharArray()).contains(new CustomString("hamburgers".toCharArray())) == false &&
				new CustomString("hamburger".toCharArray()).contains(new CustomString("shamburger".toCharArray())) == false &&
				new CustomString("hamburger".toCharArray()).contains(new CustomString("ham".toCharArray())) == true &&
				new CustomString("redshoeshoeshoe".toCharArray()).contains(new CustomString("shoe".toCharArray())) == true &&
				new CustomString("ttuixcfg".toCharArray()).contains(new CustomString("xcf".toCharArray())) == true &&
				new CustomString("hamburger".toCharArray()).contains(null) == false ) ) {
			Error("contains does not work as intended!");
		}
		
		// test toUpperCase()
		CustomString testUC = hello.toUpperCase();
		if (testUC.equals(uppercaseHello) == false) {
			Error("toUpperCase does not work as intended!");
		}
		
		// test toString()
		if (!hello.toString().equals("hello")) {
			Error("toString does not work as intended!");
		}
		
		// test toCharArray()
		char[] testCa = hello.toCharArray();
		if(testChars.equals(testCa)) {
			Error("toCharArray does not work as intended!");
		}
	}
	
	
	
	private static void Error(String message) {
		System.out.printf("TEST ERROR: %s", message);
		System.exit(0);
	}

}
