package Custom;
public class CustomString {

	private char[] values;
	private int size;
	
	/**
	 * DEFAULT_CAP is used as padding at the end of the values property
	 */
	public static final int DEFAULT_CAP = 10;
	
	/** 
	 * Default Constructor.
	 */
	public CustomString() {
		this.size = 0;
		this.values = new char[DEFAULT_CAP];
	}
	
	/** 
	 * Constructor with character array parameter.
	 * @param values - An array of characters that will be assigned to values propety
	 */
	public CustomString(char[] values) {
		this.size = values.length;
		this.values = new char[values.length + DEFAULT_CAP];
		
		for(int i = 0; i < values.length; i++) {
			this.values[i] = values[i];
		}
	}
	
	public int getSize() { return this.size; }
	
	public int length() { return this.size; }
	
	public int getCapacity() { return this.values.length; }
	
	public void append(char[] values) {
		if (values != null && values.length > 0) {
			int remainingSpace = this.values.length - this.size;
			if (remainingSpace < values.length) {
				this.resize(this.size + (values.length - remainingSpace) + DEFAULT_CAP);
			}
			for (int i = 0; i < values.length; i++) {
				this.values[this.size + i] = values[i];
			}
			this.size += values.length;
		}
	}
	
	public boolean isEmpty() {
		return this.size == 0;
	}
	
	public char charAt(int index) {
		return ( (index < size && index >= 0) ? this.values[index] : 0 );
	}
	
	public boolean equals(Object anObject) {
		if (anObject != null && anObject instanceof CustomString) {
			CustomString tempCustomString = (CustomString) anObject;
			for (int i = 0; i < tempCustomString.size; i++) {
				if (this.values[i] != tempCustomString.values[i]) return false;
			}
			return true;
		}
		return false;
	}
	
	public boolean equalsIgnoreCase(CustomString anotherCString) {
		if (anotherCString != null) {
			CustomString tempThis = this.toUpperCase();
			CustomString tempOther = anotherCString.toUpperCase();
			return tempThis.equals(tempOther);
		}
		return false;
	}
	
	public int compareTo(CustomString anotherCString) {
		if (anotherCString == null) return 1;
		for (int i = 0; i < (this.size < anotherCString.size ? this.size : anotherCString.size); i++) {
			if (this.values[i] < anotherCString.values[i]) return -1;
			else if (this.values[i] > anotherCString.values[i]) return 1;
		}
		// If they are the same size, return zero. If 'this' is longer return 1, else return -1
		return  (this.size == anotherCString.size) ? 0 : (this.size > anotherCString.size ? 1 : -1);
	}
	
	public boolean startsWith(CustomString prefix) {
		if (prefix != null && prefix.size <= this.size) {
			for (int i = 0; i < prefix.size; i++) {
				if (this.values[i] != prefix.values[i]) return false;
			}
			return true;
		}
		return false;
	}
	
	public boolean endsWith(CustomString suffix) {
		if (suffix != null && suffix.size <= this.size) {
			for (int i = 0; i < suffix.size; i++) {
				// Check each character in 'this' with an adjusted index shifted by the suffix size
				if (this.values[(this.size - suffix.size) + i] != suffix.values[i]) return false;
			}
			return true;
		}
		return false;
	}
	
	public int indexOf(CustomString anotherCString) {
		if (anotherCString != null && this.size >= anotherCString.size) {
			for (int i = 0; i < this.size; i++) {
				if (this.values[i] == anotherCString.values[0]) {
					int j = 1;
					for (; j < anotherCString.size; j++) {
						if (this.values[i+j] != anotherCString.values[j]) break;
					}
					if (j == anotherCString.size) return i;
				}
			}
		}
		return -1;
	}
	
	public CustomString substring(int beginIndex, int endIndex) {
		if (beginIndex > -1 && endIndex <= this.size) {
			char[] newChars = new char[endIndex - beginIndex];
			for (int i = 0; i < newChars.length; i++) {
				newChars[i] = this.values[beginIndex + i];
			}
			return new CustomString(newChars);
		}
		return null;
	}
	
	public CustomString replace(CustomString target, CustomString replacement) {
		CustomString newCString = new CustomString(this.values);
		
		int index = newCString.indexOf(target);
		while (index != -1) {
			CustomString prefix = newCString.substring(0, index);
			prefix.append(replacement.toCharArray());
			CustomString suffix = newCString.substring( (index + target.size), newCString.size );
			prefix.append(suffix.toCharArray());
			newCString = prefix;
			index = newCString.indexOf(target);
		}
		
		return newCString;
	}
	
	public void resize(int newCap) {
		if (newCap > this.values.length) {
			char[] newValues = new char[newCap];
			for (int i = 0; i < this.size; i++) {
				newValues[i] = this.values[i];
			}
			this.values = newValues;
		}
	}
	
	public boolean contains(CustomString anotherCString) {
		if (anotherCString != null && anotherCString.size <= this.size) {
			return this.indexOf(anotherCString) > -1;
		}
		return false;
	}
	
	public CustomString toUpperCase() {
		char[] newChars = new char[this.size];
		for (int i = 0; i < this.size; i++) {
			// Just some ASCII magic ;) That's right, no Character class dependencies..
			newChars[i] = (char) ((this.values[i] >= 97 && this.values[i] <= 122) ?
					               this.values[i] - 32 : this.values[i]);
		}
		return new CustomString(newChars);
	}
	
	public String toString() {
		String string = "";
		for(int i = 0; i < this.size; i++) {
			string += values[i];
		}
		return string;
	}
	
	public char[] toCharArray() {
		char[] newArr = new char[this.size];
		for(int i = 0; i < this.size; i++) {
			newArr[i] = this.values[i];
		}
		return newArr;
	}
}
