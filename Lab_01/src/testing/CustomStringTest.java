package testing;

import Custom.CustomString;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CustomStringTest {
	
	@Test
	void testCompareTo() {
		CustomString s0 = new CustomString("helloWorld".toCharArray());
		CustomString s1 = new CustomString("HelloWorld".toCharArray());
		int result0 = s0.compareTo(s1);
		assertEquals(result0, 1);
		
		CustomString s2 = new CustomString("abbbbbbbbbbbbb".toCharArray());
		CustomString s3 = new CustomString("aa".toCharArray());
		int result1 = s3.compareTo(s2);
		assertEquals(result1, -1);
		
		CustomString s4 = new CustomString("abcedf".toCharArray());
		CustomString s5 = new CustomString("abcedf".toCharArray());
		int result2 = s4.compareTo(s5);
		assertEquals(result2, 0);
	}

}
