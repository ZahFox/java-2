package models;

import java.io.Serializable;

public class SecretAgent implements Serializable {

	// Exception Messages //
	public static final String FIRST_NAME_NULL = "A SecretAgent's `firstName` can't be NULL.";
	public static final String FIRST_NAME_EMPTY = "A SecretAgent's `firstName` can't be an empty String.";
	public static final String LAST_NAME_NULL = "A SecretAgent's `lastName` can't be NULL.";
	public static final String LAST_NAME_EMPTY = "A SecretAgent's `lastName` can't be an empty String.";
	public static final String ID_NULL = "A SecretAgent's `id` can't be NULL.";
	public static final String ID_EMPTY = "A SecretAgent's `id` can't be an empty String.";
	
	// Instance Variables //
	private String firstName;
	private String lastName;
	private String id;
	
	// Constructor //
	public SecretAgent(String lastName, String firstName, String id) {
		this.setLastName(lastName);
		this.setFirstName(firstName);
		this.setId(id);
	}
	
	@Override
	public String toString() {
		return String.format("Secret Agent:%nId: %s%nName: %s %s",
				this.getId(), this.getFirstName(), this.getLastName());
	}
	
	// Getters and Setters //
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) throws IllegalArgumentException, NullPointerException {
		if (firstName == null) throw new NullPointerException(FIRST_NAME_NULL);
		else if (firstName.equals("")) throw new IllegalArgumentException(FIRST_NAME_EMPTY);
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) throws IllegalArgumentException, NullPointerException {
		if (lastName == null) throw new NullPointerException(LAST_NAME_NULL);
		else if (lastName.equals("")) throw new IllegalArgumentException(LAST_NAME_EMPTY);
		this.lastName = lastName;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) throws IllegalArgumentException, NullPointerException {
		if (id == null) throw new NullPointerException(ID_NULL);
		else if (id.equals("")) throw new IllegalArgumentException(ID_EMPTY);
		this.id = id;
	}
}
