package models;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import interfaces.CipherIO;

public class CaesarCipher implements CipherIO {
	
	// Character Shifting Limits //
	public static final int MIN_CHAR   = 0;
	public static final int MAX_CHAR   = 127;
	public static final int MAX_SHIFT  = 9001;
	public static final int MIN_SHIFT  = 6;
	
	// Exception Messages //
	public static final String SENDER_NULL         = "A CaesarCipher's `Sender` can't be NULL.";
	public static final String RECEIVER_NULL       = "A CaesarCipher's `Receiver` can't be NULL.";
	public static final String MSG_NULL            = "A CaesarCipher's `msg` can't be NULL.";
	public static final String MSG_EMPTY           = "A CaesarCipher's `msg` can't be an empty String.";
	public static final String OFFSET_INVALID      = "A CaesarCipher's `offset` can't be 0.";
	public static final String NEXT_OFFSET_INVALID = "A CaesarCipher's `nextOffset` can't be 0.";
	public static final String MSG_FILE_NULL       = "A CaesarCipher's `msgFile` can't be NULL.";
	public static final String MSG_FILE_INVALID    = "A CaesarCipher's `msgFile` must a valid file that exists and is readable.";
	
	// Instance Variables //
	private SecretAgent sender;
	private SecretAgent receiver;
	private String msg;
	private int offset;
	private int nextOffset;
	private File msgFile;

	// Constructors //
	    // >> For Writing a Message
	public CaesarCipher(SecretAgent sender, SecretAgent receiver, File msgFile, int offset, String msg) {
		this.setSender(sender);
		this.setReceiver(receiver);
		this.setMsgFile(msgFile);
		this.setOffset(offset);
		this.setMsg(msg);
	}
	    // >> For Reading an Encrypted Message
	public CaesarCipher(File msgFile, int offset) {
		this.setMsgFile(msgFile);
		this.setOffset(offset);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("***Message Information***");
		sb.append(String.format("%nSender:%n%s", this.getSender().toString().replaceAll("(?m)^", "\t")));
		sb.append(String.format("%nReceiver:%n%s", this.getReceiver().toString().replaceAll("(?m)^", "\t")));
		sb.append(String.format("%nMessage:%n%s", this.getMsg().replaceAll("(?m)^", "\t")));
		sb.append(String.format("%nNext Offset:%n\t%s", this.getNextOffset()));
		return sb.toString();
	}
	
	// CipherIO Implementation //
	@Override
	public void writeToBinaryFile() throws FileNotFoundException, IOException {
		int nextOffset = ThreadLocalRandom.current().nextInt(MIN_SHIFT, MAX_SHIFT + 1);
		this.setNextOffset(nextOffset);
		char[] encryptedMessage = this.getMsg().toCharArray();
		
		for (int i = 0; i < encryptedMessage.length; i++) {
			encryptedMessage[i] = shiftChar(encryptedMessage[i], this.getOffset());
		}
		
		String message = new String(encryptedMessage);
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(this.getMsgFile()));
		oos.writeObject(this.getSender());
		oos.writeObject(this.getReceiver());
		oos.writeObject(message);
		oos.writeInt(nextOffset);
		oos.close();
	}
	
	@Override
	public int readFromBinaryFile() throws FileNotFoundException, ClassNotFoundException, EOFException, IOException  {
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(this.getMsgFile()));
		
		this.setSender( (SecretAgent)ois.readObject() );
		this.setReceiver( (SecretAgent)ois.readObject() );
		char[] encryptedMessage = ( (String)ois.readObject() ).toCharArray();
		int nextOffset = ois.readInt();
		ois.close();

		for (int i = 0; i < encryptedMessage.length; i++) {
			encryptedMessage[i] = shiftChar(encryptedMessage[i], this.getOffset() * -1);
		}
		
		this.setMsg(new String(encryptedMessage));
		this.setNextOffset(nextOffset);
		return nextOffset;
	}
	
	// This Method Assumes That the Character is ASCII Encoded
	public static final char shiftChar(char c, int offset) {
		if (c >= MIN_CHAR && c <= MAX_CHAR && offset != 0) {
			if (offset > 0) {
				offset = offset % ((MAX_CHAR + 1) - MIN_CHAR);
				int val = c + offset;
				if (val > MAX_CHAR)
					return (char)((MIN_CHAR - 1) + (val - MAX_CHAR));
				else
					return (char)val;
			}
			else if (offset < 0) {
				offset = ((offset * -1) % ((MAX_CHAR + 1) - MIN_CHAR));
				int val = c - offset;
				if (val < MIN_CHAR)
					return (char)((MAX_CHAR + 1) - (offset - (c - MIN_CHAR)));
				else
					return (char)val;
			} 
		}
		return c;
	}
	
	// Getters and Setters //
	public SecretAgent getSender() {
		return sender;
	}
	
	public void setSender(SecretAgent sender) throws NullPointerException {
		if (sender == null) throw new NullPointerException(SENDER_NULL);
		else this.sender = sender;
	}
	
	public SecretAgent getReceiver() {
		return receiver;
	}
	
	public void setReceiver(SecretAgent receiver) throws NullPointerException {
		if (receiver == null) throw new NullPointerException(RECEIVER_NULL);
		else this.receiver = receiver;
	}
	
	public String getMsg() {
		return msg;
	}
	
	public void setMsg(String msg) throws IllegalArgumentException, NullPointerException {
		if (msg == null) throw new NullPointerException(MSG_NULL);
		else if (msg.equals("")) throw new IllegalArgumentException(MSG_EMPTY);
		this.msg = msg;
	}
	
	public int getOffset() {
		return offset;
	}
	
	public void setOffset(int offset) throws IllegalArgumentException {
		if (offset == 0) throw new IllegalArgumentException(MSG_EMPTY);
		else this.offset = offset;
	}
	
	public int getNextOffset() {
		return nextOffset;
	}
	
	public void setNextOffset(int nextOffset) throws IllegalArgumentException {
		if (nextOffset == 0) throw new IllegalArgumentException(MSG_EMPTY);
		else this.nextOffset = nextOffset;
	}
	
	public File getMsgFile() {
		return msgFile;
	}
	
	public void setMsgFile(File msgFile) throws IllegalArgumentException, NullPointerException {
		if (msgFile == null) throw new NullPointerException(MSG_FILE_NULL);
		else if (!msgFile.exists() || !msgFile.isFile() || !msgFile.canRead() || !msgFile.canWrite())
			throw new IllegalArgumentException(MSG_FILE_INVALID);
		else this.msgFile = msgFile;
	}
	
}
