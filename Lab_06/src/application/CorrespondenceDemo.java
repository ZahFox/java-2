package application;

import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import models.CaesarCipher;
import models.SecretAgent;


// QUESTION 1 : In the class diagram for CaesarCipher the readFromBinaryFile returns void,
// but this class expects it to return a new offset as an integer..?

// QUESTION 2 : The contructor may need .createNewFile() added to the file objects
public class CorrespondenceDemo {
	
	private SecretAgent john;
	private SecretAgent vlad;
	private int initialOffset;
	
	private File fileFromJohn;
	private File fileFromVlad;
	private File file2FromJohn;
	
	public static void main(String[] args) {
		try {
			CorrespondenceDemo c = new CorrespondenceDemo(8);		
			c.runScenario();
		}
		catch (FileNotFoundException e) {
			Error(e, "The Application Has Encountered An Error Due To Trying To Find A File That Does Not Exist!");
		}
		catch (ClassNotFoundException e) {
			Error(e, "The Application Has Encountered An Error Due To Trying To Read The Wrong Kind Of Data From A File!");
		}
		catch (EOFException e) {
			Error(e, "The Application Has Encountered An Error Due To Trying To Read Past The End Of A File!");
		}
		catch (IOException e) {
			Error(e, "The Application Has Encountered An Error While Trying To Read From Or Write To A File!");
		}
		catch (Exception e) {
			Error(e, "The Application Has Encountered An Unexpected Error!");
		}
		finally {
			System.exit(0);
		}
	}
	
	public void runScenario() throws FileNotFoundException, IOException, ClassNotFoundException{
		System.out.println("***Starting Correspondence***");
		System.out.println();
		
		//Message 1
		System.out.println("*****MESSAGE 1********");
		String msg = "Hi Vladamir, don't put missles in Cuba!";
		writeMsg(john, vlad, msg, fileFromJohn, this.initialOffset);
		int nextOffset = readMsg(fileFromJohn, this.initialOffset);
		
		//Message 2
		System.out.println("*****MESSAGE 2********");
		msg = "Thanks comrade! I will send a set of nesting dolls to show my appreciation";
		writeMsg(vlad, john, msg, fileFromVlad, nextOffset);
		nextOffset = readMsg(fileFromVlad, nextOffset);
		
		//Message 3
		System.out.println("*****MESSAGE 3********");
		msg = "Thanks for the nesting dolls.";
		writeMsg(john, vlad, msg, file2FromJohn, nextOffset);
		nextOffset = readMsg(file2FromJohn, nextOffset);
		
		System.out.println("***Ending Correspondence***");
		fileFromJohn.delete();
		fileFromVlad.delete();
		file2FromJohn.delete();
	}
	
	public CorrespondenceDemo(int initialOffset) throws IOException{
		this.initialOffset = initialOffset;
		
		john = new SecretAgent("Doe", "John", "12345");
		vlad = new SecretAgent("Mendelev", "Vladimir", "54321");
		
		fileFromJohn = new File("src/testing/msgFromJohn.enc");
		fileFromJohn.createNewFile();
		fileFromVlad = new File("src/testing/msgFromVlad.enc");
		fileFromVlad.createNewFile();
		file2FromJohn = new File("src/testing/msg2FromJohn.enc");
		file2FromJohn.createNewFile();	
	}
	
	private void writeMsg(SecretAgent sender, SecretAgent receiver, String msg, File msgFile, int offset) throws FileNotFoundException, IOException {
		System.out.printf("***Writing message from %s to %s***%n", sender.getFirstName(), receiver.getFirstName());
		
		System.out.println("Message:");
		System.out.println("\t" + msg);
		
		System.out.println("Offset:");
		System.out.println("\t" +  offset);		
		
		CaesarCipher cipher = new CaesarCipher(sender, receiver,msgFile, offset, msg);
		
		cipher.writeToBinaryFile();

		System.out.println("***Done writing message***\n\n");
	}
	
	//reads the message and returns the new offset.
	private int readMsg(File msgFile, int offset) throws FileNotFoundException, ClassNotFoundException, EOFException, IOException{
		System.out.println("***Reading message***");		
		CaesarCipher cipher = new CaesarCipher(msgFile, offset);
		int nextOffset = 0;
		
		nextOffset = cipher.readFromBinaryFile();
		System.out.println(cipher.toString());
		
		System.out.println("***Done reading message***\n\n");
		
		return nextOffset;
	}
	
	private static final void Error(Exception e, String message) {
		System.err.printf("%s:%n\t%s",e.getMessage(), message);
	}
}
