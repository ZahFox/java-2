package testing;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.jupiter.api.Test;
import models.SecretAgent;
import models.CaesarCipher;

class TestCaesarCipher {


	private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	
	@Test
	void testContructors() throws IOException {
		File testFile = new File("src/testing/test_file_001.dat");
		testFile.createNewFile();
		// Test the Constructor for Writing a Message
		CaesarCipher writer = new CaesarCipher(
		    new SecretAgent("TEST_SENDER", "TEST_SENDER", "001"),
		    new SecretAgent("TEST_RECEIVER", "TEST_RECEIVER", "002"),
		    testFile, 2, "THIS IS FOR A TEST"
		);
		// Test the Constructor for Reading an Encrypted Message
		CaesarCipher reader = new CaesarCipher(testFile, 2);
		testFile.delete();
	}
	
	@Test // The Following Tests Assumes that the MIN and MAX Char values include at least 32-126 from the ASCII table..
	void testShiftChar() {
		char testOne = 'b';
		testOne = CaesarCipher.shiftChar(testOne, 10);
		assertEquals('l', testOne);
		testOne = CaesarCipher.shiftChar(testOne, -10);
		assertEquals('b', testOne);
		
		char testTwo = '{';
		testTwo = CaesarCipher.shiftChar(testTwo, (CaesarCipher.MAX_CHAR + 1) - CaesarCipher.MIN_CHAR);
		assertEquals('{', testTwo);
		testTwo = CaesarCipher.shiftChar(testTwo, -1 * ((CaesarCipher.MAX_CHAR + 1) - CaesarCipher.MIN_CHAR));
		assertEquals('{', testTwo);
		
		char testThree = '6';
		testThree = CaesarCipher.shiftChar(testThree, (((CaesarCipher.MAX_CHAR + 1) - CaesarCipher.MIN_CHAR) * 10000) + 20);
		assertEquals('J', testThree);
		testThree = CaesarCipher.shiftChar(testThree, -1 * ((((CaesarCipher.MAX_CHAR + 1) - CaesarCipher.MIN_CHAR) * 10000) + 20));
		assertEquals('6', testThree);
	}
	
	@Test
	void testBinaryFileOperations() throws IOException, ClassNotFoundException {
		SecretAgent agent1 = new SecretAgent("TEST_SENDER", "TEST", "42");
		SecretAgent agent2 = new SecretAgent("TEST_RECEIVER", "TEST", "33");
		
		for (int i = 0; i < 1000; i++) {
			int initialOffset = ThreadLocalRandom.current().nextInt(CaesarCipher.MIN_SHIFT, CaesarCipher.MAX_SHIFT + 1);;
			File file = new File(String.format("src/testing/test_file_%d.dat", i));
			file.createNewFile();
			String message = randomAlphaNumeric(initialOffset);
			
			CaesarCipher writeCipher = new CaesarCipher(agent1, agent2, file, initialOffset, message);
			writeCipher.writeToBinaryFile();
			int writtenNextOffset = writeCipher.getNextOffset();
			assertEquals(initialOffset, writeCipher.getOffset());
			assertEquals(message, writeCipher.getMsg());
			assertEquals(file, writeCipher.getMsgFile());
			assertEquals(agent1, writeCipher.getSender());
			assertEquals(agent2, writeCipher.getReceiver());
			assertEquals(true, writeCipher.getNextOffset() > 0);
			
			CaesarCipher readCipher = new CaesarCipher(file, initialOffset);
			assertEquals(initialOffset, readCipher.getOffset());
			assertEquals(file, readCipher.getMsgFile());
			int readNextOffset = readCipher.readFromBinaryFile();
			assertEquals(true, readCipher.getNextOffset() > 0);	
			assertEquals(writtenNextOffset, readNextOffset);
			assertEquals(message, readCipher.getMsg());
			
			file.delete();
		}
	}
	

	private static final String randomAlphaNumeric(int count) {
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}
}
