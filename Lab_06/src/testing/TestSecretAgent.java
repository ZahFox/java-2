package testing;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import models.SecretAgent;

class TestSecretAgent {

	@Test
	void testSettersAndContructor() {
		Throwable e1 = assertThrows(NullPointerException.class, () -> {
			SecretAgent agent = new SecretAgent(null, "test", "test");
		});
		assertEquals(SecretAgent.LAST_NAME_NULL, e1.getMessage());
		
		Throwable e2 = assertThrows(IllegalArgumentException.class, () -> {
			SecretAgent agent = new SecretAgent("", "test", "test");
		});
		assertEquals(SecretAgent.LAST_NAME_EMPTY, e2.getMessage());
		
		Throwable e3 = assertThrows(NullPointerException.class, () -> {
			SecretAgent agent = new SecretAgent("test", null, "test");
		});
		assertEquals(SecretAgent.FIRST_NAME_NULL, e3.getMessage());
		
		Throwable e4 = assertThrows(IllegalArgumentException.class, () -> {
			SecretAgent agent = new SecretAgent("test", "", "test");
		});
		assertEquals(SecretAgent.FIRST_NAME_EMPTY, e4.getMessage());
		
		Throwable e5 = assertThrows(NullPointerException.class, () -> {
			SecretAgent agent = new SecretAgent("test", "test", null);
		});
		assertEquals(SecretAgent.ID_NULL, e5.getMessage());
		
		Throwable e6 = assertThrows(IllegalArgumentException.class, () -> {
			SecretAgent agent = new SecretAgent("test", "test", "");
		});
		assertEquals(SecretAgent.ID_EMPTY, e6.getMessage());
		
		final SecretAgent agent = new SecretAgent("test", "test", "42");
		agent.setLastName("Johnson");
		agent.setFirstName("Carl");
		agent.setId("182");
		
		Throwable e7 = assertThrows(NullPointerException.class, () -> {
			agent.setLastName(null);
		});
		assertEquals(SecretAgent.LAST_NAME_NULL, e7.getMessage());
		
		Throwable e8 = assertThrows(IllegalArgumentException.class, () -> {
			agent.setLastName("");
		});
		assertEquals(SecretAgent.LAST_NAME_EMPTY, e8.getMessage());
		
		Throwable e9 = assertThrows(NullPointerException.class, () -> {
			agent.setFirstName(null);
		});
		assertEquals(SecretAgent.FIRST_NAME_NULL, e9.getMessage());
		
		Throwable e10 = assertThrows(IllegalArgumentException.class, () -> {
			agent.setFirstName("");
		});
		assertEquals(SecretAgent.FIRST_NAME_EMPTY, e10.getMessage());
		
		Throwable e11 = assertThrows(NullPointerException.class, () -> {
			agent.setId(null);
		});
		assertEquals(SecretAgent.ID_NULL, e11.getMessage());
		
		Throwable e12 = assertThrows(IllegalArgumentException.class, () -> {
			agent.setId("");
		});
		assertEquals(SecretAgent.ID_EMPTY, e12.getMessage());
	}
	
	@Test
	void testToString() {
		final String expected = String.format("Secret Agent:%nId: %s%nName: %s %s",
				"9001", "Son", "Kakarot");
		final SecretAgent agent = new SecretAgent("Kakarot", "Son", "9001");
		final String result = agent.toString();
		assertEquals(expected, result);
	}
	
}
