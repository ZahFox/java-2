package notes;

public final class AppDriver {

	public static void main(String[] args) {
		
		int[] values = {1, 4, 5, 6, 3, 4, 5};
		QuickSort.sort(values, 0, values.length - 1);
		
		for (int i : values) {
			System.out.println(i);
		}
	}

}
