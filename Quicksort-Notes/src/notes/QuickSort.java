package notes;

public final class QuickSort {

	public static final void sort(int[] values, int low, int high) {
		
		if (low >= high) return;
		
		int sortedIndex = partition(values, low, high);
		sort(values, low, sortedIndex - 1);
		sort(values, sortedIndex + 1, high);
	}
	
	private static final int partition(int[] values, int low, int high) {
		int index = low - 1;
		int pivot = values[high];
		
		for (int i = low; i < high; i++) {
			if (values[i] < pivot) {
				index++;
				int temp = values[i];
				values[i] = values[index];
				values[index] = temp;
			}
		}
		
		index++;
		values[high] = values[index];
		values[index] = pivot;
		return index;
	}
	
}
